# coding=utf-8
"""Grupo3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, patterns
from django.contrib import admin
from django.conf.urls.static import static

from CanastaOrganica import views
from settings import common

urlpatterns = [
    # MAIN URLS
    url(r'^$', views.index, name='index'),
    url(r'^login/*', views.canastaorganica_login, name='login'),
    url(r'^logout/', views.canastaorganica_logout, name='logout'),

    # REST
    url(r'^rest/productos/unidades/(?P<id_oferta>[0-9]+)', views.rest_productos_unidades,
        name='rest_productos_unidades'),
    url(r'^rest/productos/ofertas/(?P<page>[0-9]+)', views.rest_productos_ofertas,
        name='rest_productos_ofertas'),
    url(r'^rest/productos/certificados/(?P<page>[0-9]+)', views.rest_productos_certificados,
        name='rest_productos_certificados'),
    url(r'^rest/productos/ofertas_periodo/(?P<page>[0-9]+)/(?P<id_periodo>[0-9]+)',
        views.rest_productos_ofertados_periodo,
        name='rest_productos_ofertas_periodo'),
    url(r'^rest/productos/ofertas_productores/(?P<id_periodo>[0-9]+)/(?P<id_producto>[0-9]+)',
        views.rest_producto_oferta_productores,
        name='rest_producto_oferta_productores'),

    # REST - ADMIN
    url(r'^rest/productos/listar/(?P<page>[0-9]+)', views.rest_admin_productos_lista,
        name='rest_admin_productos_lista'),
    url(r'^rest/productos/crear', views.rest_admin_productos_crear,
        name='rest_admin_productos_crear'),
    url(r'^rest/productos/form/', views.rest_producto_crear_form, name='rest_producto_crear_form'),
    url(r'^admin/notificar/correos', views.enviar_correos_lista_deseos, name='enviar_correos_lista_deseos'),

    # REST - TIPOS
    url(r'^rest/tipo/unidades/listar', views.rest_tipo_unidades_lista, name='rest_tipo_unidades_lista'),

    url(r'^rest/periodo/listar', views.rest_periodo_listar, name='rest_periodo_listar'),

    url(r'^rest/carro/agregar', views.rest_carro_agregar,
        name='rest_carro_agregar'),
    url(r'^rest/carro/items/eliminar', views.rest_carro_items_eliminar, name='rest_carro_items_eliminar'),
    url(r'^rest/carro/items/no_disponible', views.rest_carro_items_no_disponibles,
        name='rest_carro_items_no_disponibles'),
    url(r'^rest/carro/items/count', views.rest_carro_items_count,
        name='rest_carro_items_count'),
    url(r'^rest/carro/items/valor', views.rest_carro_items_valor,
        name='rest_carro_items_valor'),
    url(r'^rest/carro/items/(?P<page>[0-9]+)', views.rest_carro_items,
        name='rest_carro_items'),
    url(r'^rest/carro/horarios/asignar', views.rest_carro_horarios_asignar,
        name='rest_carro_horarios_asignar'),
    url(r'^rest/carro/horarios', views.rest_carro_horarios,
        name='rest_carro_horarios'),
    url(r'^rest/carro/estado/actualizar', views.rest_carro_estado_actualizar,
        name='rest_carro_estado_actualizar'),

    url(r'^rest/horarios/listar/(?P<page>[0-9]+)', views.rest_horarios_paginado,
        name='rest_horarios_paginado'),
    url(r'^rest/horarios/cambiarEstado', views.rest_horario_cambiar_estado,
        name='rest_horario_cambiar_estado'),
    url(r'^rest/horarios/crear', views.rest_horario_crear,
        name='rest_horario_crear'),

    url(r'^rest/users/actual', views.rest_users_actual,
        name='rest_users_actual'),
    url(r'^rest/users/cliente/form/(?P<id_usuario>[0-9]+)', views.rest_users_cliente_form,
        name='rest_users_cliente_form'),
    url(r'^rest/users/productor/form/(?P<id_usuario>[0-9]+)', views.rest_users_productor_form,
        name='rest_users_productor_form'),
    url(r'^rest/users/productores/(?P<page>[0-9]+)', views.rest_users_productores,
        name='rest_users_productores'),
    url(r'^rest/users/productores/cambiarEstado', views.rest_cambiar_estado_productor,
        name='rest_cambiar_estado_productor'),

    url(r'^rest/orden/listar/(?P<page>[0-9]+)', views.rest_orden_listar,
        name='rest_orden_listar'),
    url(r'^rest/orden/cambiar_valor', views.rest_oferta_cambiar_valor, name='rest_oferta_cambiar_valor'),

    # PRODUCTO TIPO
    url(r'^rest/productos_tipo/ofertas_periodo/(?P<page>[0-9]+)/(?P<id_periodo>[0-9]+)',
        views.rest_productos_tipo_periodo, name='rest_productos_tipo_periodo'),
    url(r'^rest/productos_tipo/ofertas_periodo_all/(?P<id_periodo>[0-9]+)',
        views.rest_productos_ofertados_periodo_all, name='rest_productos_ofertados_periodo_all'),
    url(r'^rest/productos_tipo/productos/(?P<id_canasta>[0-9]+)',
        views.rest_productos_producto_tipo, name='rest_productos_producto_tipo'),

    url(r'^rest/productos_tipo/crear',
        views.rest_productos_tipo_crear, name='rest_productos_tipo_crear'),
    url(r'^rest/productos_tipo/delete',
        views.rest_productos_tipo_eliminar, name='rest_productos_tipo_eliminar'),

    url(r'^rest/productos_tipo/adicionar',
        views.rest_productos_tipo_adicionar_detalle, name='rest_productos_tipo_adicionar_detalle'),
    url(r'^rest/productos_tipo/modificar',
        views.rest_productos_tipo_modificar_detalle, name='rest_productos_tipo_modificar_detalle'),
    url(r'^rest/productos_tipo/eliminar',
        views.rest_productos_tipo_eliminar_detalle, name='rest_productos_tipo_eliminar_detalle'),

    url(r'^rest/pedido/listar/(?P<page>[0-9]+)', views.rest_pedido_productor,
        name='rest_pedido_productor'),

    # PASARELA PAGO
    url(r'^pagar', views.pagar, name='pagar'),
    url(r'^confirmarPago', views.confirmar_pago, name='confirmarPago'),

    # ADMIN
    url(r'^admin/', admin.site.urls),

    # LISTA DE DESEOS
    url(r'^rest/productos/lista_deseo',
        views.rest_productos_lista_deseo, name='rest_productos_lista_deseo'),
    url(r'^rest/lista_deseo/cambio',
        views.rest_productos_lista_deseo_cambio, name='rest_productos_lista_deseo_cambio'),
    url(r'^rest/producto_deseado',
        views.rest_adcicionar_producto_deseado, name='rest_adcicionar_producto_deseado'),

    # REST SPRINT 3
    url(r'^api/v1/usuario/login', views.rest_api_v1_usuario_login, name='rest_api_v1_usuario_login'),
    url(r'^api/v1/products', views.rest_api_v1_listar_producto_ofertados, name='rest_api_v1_listar_producto_ofertados'),
    url(r'^api/v1/cart/add', views.rest_api_v1_adicionar_producto_carro, name='rest_api_v1_adicionar_producto_carro'),
    url(r'^api/v1/cart/checkout', views.rest_api_v1_confirmar_pedido, name='rest_api_v1_confirmar_pedido'),
    url(r'^api/v1/order/schedule', views.rest_api_v1_listar_horarios_distribucion,
        name='rest_api_v1_listar_horarios_distribucion'),
    url(r'^api/v1/order', views.rest_api_v1_listar_productos_comprados, name='rest_api_v1_listar_productos_comprados'),

]

urlpatterns += patterns('',
                        url(r'^jsreverse/$', 'django_js_reverse.views.urls_js', name='js_reverse'),
                        )
urlpatterns += static(common.MEDIA_URL, document_root=common.MEDIA_ROOT)
urlpatterns += static(common.STATIC_URL, document_root=common.STATIC_ROOT)
