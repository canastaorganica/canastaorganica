from common import *

# DATABASE CONFIGURATION
import dj_database_url

DATABASES = {
    'default': dj_database_url.config()
}
# END DATABASE CONFIGURATION

# ALLOWED HOSTS CONFIGURATION
# Set the heroku host for Quality Assurance
ALLOWED_HOSTS = ['*']
# END ALLOWED HOST CONFIGURATION

# S3
AWS_HEADERS = {
    'Expires': 'Thu, 31 Dec 2099 20:00:00 GMT',
    'Cache-Control': 'max-age=94608000',
}
AWS_STORAGE_BUCKET_NAME = 'canastaorganica'
AWS_ACCESS_KEY_ID = 'AKIAJ5FZKEGBUN3EKSVA'
AWS_SECRET_ACCESS_KEY = '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'

# Tell django-storages that when coming up with the URL for an item in S3 storage, keep
# it simple - just use this domain plus the path. (If this isn't set, things get complicated).
# This controls how the `static` template tag from `staticfiles` gets expanded, if you're using it.
# We also use it in the next setting.
AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
# STATICFILES_LOCATION = 'static'
# STATICFILES_STORAGE = 'CanastaOrganica.custom_storages.StaticStorage'
# STATIC_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, STATICFILES_LOCATION)

MEDIAFILES_LOCATION = 'media'
MEDIA_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, MEDIAFILES_LOCATION)
DEFAULT_FILE_STORAGE = 'CanastaOrganica.custom_storages.MediaStorage'