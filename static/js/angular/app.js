(function (angular) {
    var site = angular.module('site', [
        'ui.router',
        'oc.lazyLoad',
        'ngProgress',
        'app.routes',
        'restangular'
    ]);

    site.config(['RestangularProvider', '$httpProvider',
        function (RestangularProvider, $httpProvider) {
            // Restangular
            var $hostname = window.location.hostname;
            var $protocol = window.location.protocol;
            var port = location.port;
            var restBaseUrl = $protocol + "//" + $hostname + (port ? ":" + port : "") + "/rest";
            RestangularProvider.setBaseUrl(restBaseUrl);

            //csrf
            $httpProvider.defaults.xsrfCookieName = 'csrftoken';
            $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
        }]);

    site.controller('MainCtrl', ['$scope', '$state', 'CanastaSvc', 'UsersSvc', '$rootScope',
        function ($scope, $state, CanastaSvc, UsersSvc, $rootScope) {
            $scope.num_productos = 0;

            synchronizePromises();

            // Carga Inicial
            // -----------------------------------
            function synchronizePromises() {
                getActualUser().then(function () {
                    getNumItems().then(function () {
                        $("#id_navbar").fadeIn();
                    });
                });
            }

            function getNumItems() {
                return CanastaSvc.getNumItems().then(function (data) {
                    $scope.num_productos = data.num_productos;
                });
            }

            function getActualUser() {
                return UsersSvc.actual().then(function (data) {
                    $scope.actual_user = data.user;
                    if (data.csrf) {
                        $("#myCsrfInput").val(data.csrf);
                    }
                });
            }

            // Actions
            // -----------------------------------
            $scope.getNumItems = function () {
                return getNumItems();
            };

            $scope.getActualUser = function () {
                return getActualUser();
            };

            $scope.registroCliente = function () {
                $('#modalLogin').modal('toggle');
                $state.go("registro/cliente");
            };

            $scope.registroProductor = function () {
                $('#modalLogin').modal('toggle');
                $state.go("registro/productor");
            };

            $scope.updateConsumidor = function () {
                $state.go("registro/cliente", {idUsuario: $scope.actual_user.id});
            };

            $scope.updateProductor = function () {
                $state.go("registro/productor", {idUsuario: $scope.actual_user.id});
            };

            $scope.modalLogin = function () {
                $("#error_login").text("");
                $('#modalLogin').modal();
            };

            $scope.login = function () {
                $("#error_login").text("");
                var params = {
                    csrfmiddlewaretoken: $("#myCsrfInput").val(),
                    userid: $("#userid").val(),
                    password: $("#password").val()
                };
                $.post(Urls.login(), params, function (data) {
                    if (data.success) {
                        $("#userid").val("");
                        $("#password").val("");
                        $('#modalLogin').modal('toggle');
                        getActualUser().then(function () {
                            $scope.redireccionPorGrupo();
                        });
                        getNumItems();
                        alertify.success("Bienvenido!");
                    } else {
                        $("#error_login").text(data.mensaje);
                    }
                });
            };

            $scope.logout = function () {
                var params = {
                    csrfmiddlewaretoken: $("#myCsrfInput").val()
                };
                $.post(Urls.logout(), params, function (data) {
                    if (data.success) {
                        getActualUser().then(function () {
                            $state.go("catalogo", {page: 1, certificado: 0}, {reload: true});
                        });
                        getNumItems();
                    } else {
                        alertify.error("Error al cerrar sesión");
                    }
                });
            };

            $scope.listaDeseos = function () {
                $.get(Urls.rest_productos_lista_deseo(), function (data) {
                    $scope.productos = data;
                    $scope.$apply();
                });

                $('#modalListaDeseos').modal();
            };

            $scope.cambiarListaDeseos = function (id_producto, isModal) {
                var params = {
                    csrfmiddlewaretoken: $("#myCsrfInput").val(),
                    id_producto: id_producto
                };
                $.post(Urls.rest_productos_lista_deseo_cambio(), params, function (data) {
                    if (data.success) {
                        if (data.delete) {
                            alertify.success("Producto eliminado de la lista de deseos.");
                        } else {
                            alertify.success("Producto adicionado a la lista de deseos.");
                        }
                        if (isModal == 1) {
                            $scope.listaDeseos();
                        }
                        $rootScope.cambio(id_producto);
                    } else {
                        alertify.error("Error al adicionado el producto a la lista de deseos.");
                    }
                });
            };

            $scope.modalProductoNoExistente = function () {
                $("#modalProdAd").val("");
                $("#modalDescProdAd").val("");
                $('#modalProductoNoExistente').modal();
            };

            $scope.agregarProductoNoExistente = function () {
                $scope.mensaje = "";
                var error = false;
                if ($("#modalProdAd").val().trim() == "") {
                    error = true;
                    $scope.mensaje = "El nombre del producto es obligatorio.";
                } else if ($("#modalDescProdAd").val().trim() == "") {
                    $scope.mensaje = "La descripción es obligatoria.";
                    error = true;
                }

                if (!error) {
                    var params = {
                        csrfmiddlewaretoken: $("#myCsrfInput").val(),
                        nombre: $("#modalProdAd").val().trim(),
                        descripcion: $("#modalDescProdAd").val().trim()
                    };

                    $.post(Urls.rest_adcicionar_producto_deseado(), params, function (data) {
                        if (data.success) {
                            $('#modalProductoNoExistente').modal('toggle');
                            alertify.success("Producto Agregado.");

                        } else {
                            alertify.error("Error al crear el producto.");
                        }
                    });
                }
            };

            // Grupos
            // -----------------------------------
            $scope.esConsumidor = function () {
                if ($scope.actual_user) {
                    return jQuery.inArray("consumidor", $scope.actual_user.groups) > -1;
                }
                return false;
            };

            $scope.esProductor = function () {
                if ($scope.actual_user) {
                    return jQuery.inArray("productor", $scope.actual_user.groups) > -1;
                }
                return false;
            };

            $scope.esAdministrador = function () {
                if ($scope.actual_user) {
                    return jQuery.inArray("administrador", $scope.actual_user.groups) > -1;
                }
                return false;
            };

            $scope.redireccionPorGrupo = function (catalogo) {
                // Valida el grupo
                if ($scope.esConsumidor() && !catalogo) {
                    console.log("consumidor");
                    $state.go("catalogo", {page: 1, certificado: 0}, {reload: true});
                }
                else if ($scope.esProductor()) {
                    console.log("productor");
                    $state.go("productor/pedido", {page: 1}, {reload: true});
                }
                else if ($scope.esAdministrador()) {
                    console.log("administrador");
                    $state.go("admin/orden", {page: 1}, {reload: true});
                }
            }
        }]);
})(angular);





