(function (angular) {
    var site = angular.module('site');

    site.service('PedidosSvc', ['Restangular', function ($rest) {
        var resource = $rest.one("pedido");

        this.list = function (page, productor) {
            return resource.one("listar", page, productor).get();
        };
    }])
})(angular);