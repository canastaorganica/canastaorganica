(function (angular) {
    var site = angular.module('site');

    site.service('UsersSvc', ['Restangular', function ($rest) {
        var resource = $rest.one("users");

        this.actual = function () {
            return resource.one("actual").get();
        };

        this.clienteForm = function (idUser) {
            return resource.one("cliente").one("form", idUser).get();
        };

        this.productorForm = function (idUser) {
            return resource.one("productor").one("form", idUser).get();
        };
    }])
})(angular);