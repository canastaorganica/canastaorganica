(function (angular) {
    var site = angular.module('site');

    site.service('AdminProductosSvc', ['Restangular', function ($rest) {
        var resource = $rest.one("productos");

        this.list = function (page) {
            return resource.one("listar", page).get();
        };

        this.form = function () {
            return resource.one("form").get();
        }
    }])
})(angular);