(function (angular) {
    var site = angular.module('site');

    site.service('ProductosSvc', ['Restangular', function ($rest) {
        var resource = $rest.one("productos");

        this.list = function (page, certificado) {
            if (certificado) {
                return resource.one("certificados", page).get();
            } else {
                return resource.one("ofertas", page).get();
            }
        };
    }])
})(angular);