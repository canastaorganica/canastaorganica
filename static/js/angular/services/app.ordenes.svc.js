(function (angular) {
    var site = angular.module('site');

    site.service('OrdenesSvc', ['Restangular', function ($rest) {
        var resource = $rest.one("orden");

        this.list = function (page) {
            return resource.one("listar", page).get();
        };
    }])
})(angular);