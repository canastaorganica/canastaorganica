(function (angular) {
    var site = angular.module('site');

    site.service('CanastaSvc', ['Restangular', function ($rest) {
        var resource = $rest.one("carro");

        this.getNumItems = function () {
            return resource.one("items").one("count").get();
        };

        this.getValorItems = function () {
            return resource.one("items").one("valor").get();
        };

        this.list = function (page) {
            return resource.one("items", page).get();
        };

        this.horarios = function () {
            return resource.one("horarios").get();
        };
    }])
})(angular);