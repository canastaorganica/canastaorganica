(function (angular) {
    var site = angular.module('site');

    site.service('TiposSvc', ['Restangular', function ($rest) {
        var resource = $rest.one("tipo");

        this.unidades = function () {
            return resource.one("unidades").one("listar").get();
        };
    }])
})(angular);