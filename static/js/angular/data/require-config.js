var requireConfig = {
    "catalogo": [
        "../static/js/angular/services/app.productos.svc.js",
        "../static/js/angular/controllers/app.catalogo.ctrl.js"
    ],
    "canasta": [
        "../static/js/angular/controllers/app.canasta.ctrl.js"
    ],
    "registro/cliente": [
        "../static/js/angular/controllers/app.registro.ctrl.js"
    ],
    "registro/productor": [
        "../static/js/angular/controllers/app.registro.ctrl.js"
    ],

    "productor/pedido": [
        "../static/js/angular/services/app.pedidos.svc.js",
        "../static/js/angular/controllers/app.pedido.ctrl.js"
    ],

    "admin/precio": [
        "../static/js/angular/controllers/app.precio.ctrl.js"
    ],
    "admin/horario": [
        "../static/js/angular/controllers/app.horario.ctrl.js"
    ],
    "admin/orden": [
        "../static/js/angular/services/app.ordenes.svc.js",
        "../static/js/angular/controllers/app.orden.ctrl.js"
    ],
    "admin/productoTipo": [
        "../static/js/angular/controllers/app.productoTipo.ctrl.js"
    ],
    "admin/producto": [
        "../static/js/angular/services/app.tipos.svc.js",
        "../static/js/angular/services/app.admin.productos.svc.js",
        "../static/js/angular/controllers/app.admin.producto.ctrl.js"
    ],
    "admin/notificar": [
        "../static/js/angular/controllers/app.admin.notificar.ctrl.js"
    ],
    "admin/productores": [
        "../static/js/angular/controllers/app.productores.ctrl.js"
    ]
};

