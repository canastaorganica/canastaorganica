/**=========================================================
 * Module: routes.js
 * App routes and resources configuration
 =========================================================*/


(function () {
    'use strict';

    var routes = angular.module('app.routes', []);

    routes.config(routesConfig);

    routesConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider'];
    function routesConfig($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) {
        // defaults to dashboard
        $urlRouterProvider.otherwise('/catalogo/1/0');

        var modules = [];
        for (var key in requireConfig) {
            if (requireConfig[key].length > 0) {
                modules.push({
                    name: key,
                    files: requireConfig[key]
                })
            }
        }
        $ocLazyLoadProvider.config({modules: modules});

        // -----------------------------------
        // MAIN STATES
        // -----------------------------------
        var mainStates = [
            'catalogo',
            'canasta',
            'registro/cliente',
            'registro/productor'
        ];
        $stateProvider
            .state(mainStates[0], stateConfigParams(mainStates[0], ['page', 'certificado']))
            .state(mainStates[1], stateConfigParams(mainStates[1], ['page']))
            .state(mainStates[2], stateConfigParams(mainStates[2], [], ['idUsuario']))
            .state(mainStates[3], stateConfigParams(mainStates[3], [], ['idUsuario']));

        // -----------------------------------
        // PRODUCTOR STATES
        // -----------------------------------
        var productorStates = [
            'productor/pedido'
        ];
        $stateProvider
            .state(productorStates[0], stateConfigParams(productorStates[0], ['page']));

        // -----------------------------------
        // ADMIN STATES
        // -----------------------------------
        var adminStates = [
            'admin/precio',
            'admin/horario',
            'admin/orden',
            'admin/productoTipo',
            'admin/producto',
            'admin/notificar',
            'admin/productores'
        ];
        $stateProvider
            .state(adminStates[0], stateConfigParams(adminStates[0], ['page'], []))
            .state(adminStates[1], stateConfigParams(adminStates[1], ['page'], []))
            .state(adminStates[2], stateConfigParams(adminStates[2], ['page']))
            .state(adminStates[3], stateConfigParams(adminStates[3], ['page']))
            .state(adminStates[4], stateConfigParams(adminStates[4], ['page']))
            .state(adminStates[5], stateConfigResolve(adminStates[5]))
            .state(adminStates[6], stateConfigParams(adminStates[6], ['page']));
    }

    // SATATE FUNCTIONS
    function stateConfigBase(stateName) {
        return {
            url: "/" + stateName,
            templateUrl: '../static/html/' + stateName + '.html'
        }
    }

    function stateConfigResolve(stateName) {
        var config = stateConfigBase(stateName);
        config['resolve'] = {
            deps: ["$ocLazyLoad", "ngProgressFactory", function ($ocLazyLoad, ngProgressFactory) {
                var ngProgress = ngProgressFactory.createInstance();
                ngProgress.setColor('#ffffff');
                ngProgress.start();
                return $ocLazyLoad.load({serie: true, files: requireConfig[stateName]}).then(function () {
                    ngProgress.complete();
                });
            }]
        };
        return config;
    }

    function stateConfigParams(stateName, paramsNames, paramsNames2) {
        var config = stateConfigResolve(stateName);
        // URL Params
        if (paramsNames) {
            var params = '';
            paramsNames.forEach(function (paramName) {
                params += ('/:' + paramName);
            });
            config['url'] = config['url'] + params;
        }
        // Hidden Params
        if (paramsNames2) {
            var params2 = {};
            paramsNames2.forEach(function (paramName) {
                params2[paramName] = null;
            });
            config['params'] = params2;
        }
        return config;
    }
})();