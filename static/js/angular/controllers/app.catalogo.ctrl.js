(function (angular) {
    var site = angular.module('site');

    site.controller('CatalogoCtrl', ['$scope', '$state', '$stateParams', 'ProductosSvc', '$rootScope',
        function ($scope, $state, $stateParams, ProductosSvc, $rootScope) {
            $scope.redireccionPorGrupo(true);

            // Paginación
            $scope.page = 1;

            if ($stateParams.page) {
                if ($stateParams.page < 1) {
                    $state.go("catalogo", {page: 1, certificado: 0});
                    return;
                }
                $scope.page = $stateParams.page;
            }

            if ($stateParams.certificado) {
                $scope.certificado = $stateParams.certificado == 1 ? true : false;
            }

            synchronizePromises();

            // Carga Inicial
            // -----------------------------------
            function synchronizePromises() {
                getListProductos($scope.page, $scope.certificado).then(function () {
                    setTimeout(function () {
                        $(".open-AdicionarCanasta").click(function () {
                            $(".modal-body #modal-nombre").text($(this).data('nombre'));
                            $(".modal-body #idProducto").text($(this).data('id_producto'));
                            $(".modal-body #precioUnitario").text($(this).data('valor_venta'));
                            $(".modal-body #modal-tipoUnidad").text($(this).data('tipo_unidad') + "s");
                            $(".modal-body #modal-cantidadDsiponible").text($(this).data('cantidad_disponible'));

                            $(".modal-body #resultado").text($(this).data('valor_venta'));
                            $(".modal-body #cantidad").val(1);
                            $(".modal-body #mensaje").text('');
                            $('#adicionarCanasta').modal('show');
                        });

                        $scope.ofertas.forEach(function (oferta) {
                            $("#wrap_prod" + oferta.id).hover(
                                function () {
                                    var unidadesSlctr = $("#unidades_prod" + oferta.id);
                                    $.get(Urls.rest_productos_unidades(oferta.id),
                                        function (data) {
                                            var unidadesDisponibles = data.unidades_disponibles;
                                            var tipoUnidad = ' ' + oferta.producto.tipoUnidad.nombre;
                                            if (unidadesDisponibles != 1) {
                                                tipoUnidad += "s";
                                            }
                                            unidadesSlctr.text(unidadesDisponibles + tipoUnidad);
                                        });

                                    //Consultamos los productos del carro
                                    if (oferta.producto.tipoProducto.id == 2) {
                                        $.get(Urls.rest_productos_producto_tipo(oferta.id),
                                            function (dataProd) {
                                                oferta.detalles = dataProd;
                                                $scope.$apply();
                                            });
                                    }

                                    $("#desc_prod" + oferta.id).stop(true, true);
                                    $("#img_prod" + oferta.id).stop(true, true).fadeOut(function () {
                                        $("#desc_prod" + oferta.id).stop(true, true).show();
                                    });
                                }, function () {
                                    $("#img_prod" + oferta.id).stop(true, true);
                                    $("#desc_prod" + oferta.id).stop(true, true).fadeOut(function () {
                                        $("#img_prod" + oferta.id).stop(true, true).show();
                                    });
                                }
                            );
                        });
                    }, 0);
                });
            }

            function getListProductos(page, certificado) {
                return ProductosSvc.list(page, certificado).then(function (data) {
                    if (page > data.num_pages) {
                        $state.go("catalogo", {page: data.num_pages, certificado: 0});
                        return;
                    }
                    $scope.ofertas = data.ofertas;
                    $scope.num_siguiente = data.num_siguiente;
                    $scope.num_anterior = data.num_anterior;
                    $scope.num_pages = new Array(data.num_pages);
                    $scope.user_athenticated = data.user_athenticated;
                    // Remove loading and display catálogo
                    $("#id_loading").hide();
                    $("#id_catalogo_wrapper").show();
                });
            }

            // Actions
            // -----------------------------------
            $scope.agregarCarro = function () {
                // Limpia el mensaje
                $(".modal-body #mensaje").text('');
                var idP = $('#idProducto')[0].innerText;
                var cantidadP = $('#cantidad').val();
                $.get(Urls.rest_productos_unidades(idP), function (data) {
                    var unidadesDisponibles = data.unidades_disponibles;
                    // Validación de disponibilidad
                    if (unidadesDisponibles >= cantidadP) {
                        var params = {
                            csrfmiddlewaretoken: $("#myCsrfInput").val(),
                            idProducto: idP,
                            cantidad: cantidadP
                        };
                        $.post(Urls.rest_carro_agregar(), params, function (data) {
                            if (data.success) {
                                $scope.getNumItems();
                                alertify.success("Producto agregado al carro");
                            } else {
                                alertify.error("Error al agregar el producto al carro");
                            }
                        });
                        $('#adicionarCanasta').modal('toggle');
                    }
                    else {
                        $(".modal-body #mensaje").text('No hay ' + cantidadP + ' unidades disponibles');
                    }
                });
            };

            $scope.filtroCertificado = function () {
                if ($scope.certificado == false) {
                    $scope.certificado = true;
                    $state.go("catalogo", {page: 1, certificado: 1});
                }
            };

            $scope.filtroTodos = function () {
                if ($scope.certificado == true) {
                    $scope.certificado = false;
                    $state.go("catalogo", {page: 1, certificado: 0});
                }
            };

            $rootScope.cambio = function (id_producto) {
                if ($scope.ofertas != undefined) {
                    $scope.ofertas.forEach(function (oferta) {
                        if (oferta.producto.id == id_producto) {
                            oferta.producto.favorito = !oferta.producto.favorito;
                            $scope.$apply();
                        }
                    });
                }
            };

        }]);
})(angular);