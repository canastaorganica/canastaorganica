(function (angular) {
    var site = angular.module('site');

    site.controller('AdminProductoresCtrl', ['$scope', '$state', '$stateParams', '$window',
        function ($scope, $state, $stateParams, $window) {
            $scope.page = 1;
            if ($stateParams.page) {
                if ($stateParams.page < 1) {
                    $state.go("admin/productores", {page: 1});
                    return;
                }
                $scope.page = $stateParams.page;
            }

            getProductores($scope.page);

            // Carga Inicial
            // -----------------------------------
            function getProductores(page) {
                $.get(Urls.rest_users_productores(page), function (data) {
                    if (page > data.num_pages) {
                        $state.go("admin/productores", {page: data.num_pages});
                        return;
                    }
                    $scope.productores = data.productores;
                    $scope.num_siguiente = data.num_siguiente;
                    $scope.num_anterior = data.num_anterior;
                    $scope.num_pages = new Array(data.num_pages);
                    $scope.$apply();
                });
            }

            // Actions
            // -----------------------------------
            $scope.cambiarEstadoProductor = function (id) {
                var params = {
                    csrfmiddlewaretoken: $("#myCsrfInput").val(),
                    idProductor: id
                };
                $.post(Urls.rest_cambiar_estado_productor(), params, function (data) {
                    if (data.success) {
                        $scope.productores.forEach(function (obj) {
                            if (obj.id == id) {
                                obj.activo = !obj.activo;
                                $scope.$apply();
                            }
                        });
                    } else {
                        alertify.error("Error al cambiar el estado del productor")
                    }
                });
            };
        }]);
})(angular);