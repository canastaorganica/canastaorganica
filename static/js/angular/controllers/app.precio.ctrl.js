(function (angular) {
    var site = angular.module('site');

    site.controller('PrecioCtrl', ['$scope', '$state', '$stateParams',
        function ($scope, $state, $stateParams) {
            $scope.page = 1;
            if ($stateParams.page) {
                if ($stateParams.page < 1) {
                    $state.go("admin/precio", {page: 1});
                    return;
                }
                $scope.page = $stateParams.page;
            }

            synchronizePromises();

            // Carga Inicial
            // -----------------------------------
            function synchronizePromises() {
                $.get(Urls.rest_periodo_listar(), function (data) {
                    $scope.semanas = [];
                    data.forEach(function (obj) {
                        $scope.semanas.push({
                            id: obj.id,
                            fechaInicio: obj.fechaInicio,
                            fechaFin: obj.fechaFin,
                            label: "Desde " + obj.fechaInicio.substring(0, 10) + " hasta " + obj.fechaFin.substring(0, 10)
                        });
                    });

                    if ($scope.semanas.length > 0) {
                        $scope.semanaSlc = $scope.semanas[0];
                        cargarOfertas();
                    }
                });
            }

            function cargarOfertas() {
                var fecha = new Date($scope.semanaSlc.fechaInicio);
                fecha = new Date(fecha.valueOf() + fecha.getTimezoneOffset() * 60000);
                fechaAct = new Date();
                $scope.activo = fecha > fechaAct;
                //Consultar las ofertas de la semana
                $.get(Urls.rest_productos_ofertas_periodo($scope.page, $scope.semanaSlc.id), function (data) {
                    if ($scope.page > data.num_pages) {
                        $state.go("admin/precio", {page: data.num_pages});
                        return;
                    }

                    $scope.ofertas = data.ofertas;
                    $scope.num_siguiente = data.num_siguiente;
                    $scope.num_anterior = data.num_anterior;
                    $scope.num_pages = new Array(data.num_pages);
                    $scope.$apply();
                });
            }
            // Actions
            // -----------------------------------
            $scope.cambioValor = function (idOrden) {
                var valorVenta = 0;
                //Se busca el valor a guardar
                for(var i = 0; i < $scope.ofertas.length; i++){
                    if($scope.ofertas[i].id == idOrden){
                        valorVenta = $scope.ofertas[i].valorVenta
                    }
                }

                var params = {
                    csrfmiddlewaretoken: $("#myCsrfInput").val(),
                    idOferta: idOrden,
                    valorVenta: valorVenta
                };

                $.post(Urls.rest_oferta_cambiar_valor(), params, function (data) {
                    if (data.success) {
                        //alertify.success("OK");
                    } else {
                        alertify.error("Error al guardar el valor");
                    }
                });
            };

            $scope.cambioSemana = function () {
                cargarOfertas();
            };

            $scope.mostrarPreciosProductores = function(idOferta){
                for (var i = 0; i < $scope.ofertas.length; i++) {
                    if ($scope.ofertas[i].id == idOferta) {
                        $scope.productoModal = $scope.ofertas[i].producto;
                    }
                }


                $.get(Urls.rest_producto_oferta_productores($scope.semanaSlc.id,
                    $scope.productoModal.id), function (data) {
                    $scope.ofertasProductores = data;
                    $scope.$apply();
                });
                $('#modalPreciosProductores').modal('toggle');
            }
        }]);
})(angular);