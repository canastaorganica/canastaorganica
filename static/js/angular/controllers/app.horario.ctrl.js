(function (angular) {
    var site = angular.module('site');

    site.controller('HorarioCtrl', ['$scope', '$state', '$stateParams', '$window',
        function ($scope, $state, $stateParams, $window) {
            $scope.page = 1;
            if ($stateParams.page) {
                if ($stateParams.page < 1) {
                    $state.go("admin/horario", {page: 1});
                    return;
                }
                $scope.page = $stateParams.page;
            }


            inicializarFecha();

            getListHorarios($scope.page);

            // Carga Inicial
            // -----------------------------------
            function getListHorarios(page) {
                $.get(Urls.rest_horarios_paginado(page), function (data) {
                    if (page > data.num_pages) {
                        $state.go("admin/horario", {page: data.num_pages});
                        return;
                    }
                    $scope.horarios = data.horarios;
                    $scope.num_siguiente = data.num_siguiente;
                    $scope.num_anterior = data.num_anterior;
                    $scope.num_pages = new Array(data.num_pages);
                    $scope.$apply();
                });
            }

            function inicializarFecha() {
                $scope.dias = [{"id": 1, "diaSemana": "Lunes"},
                    {"id": 2, "diaSemana": "Martes"},
                    {"id": 3, "diaSemana": "Miércoles"},
                    {"id": 4, "diaSemana": "Jueves"},
                    {"id": 5, "diaSemana": "Viernes"},
                    {"id": 6, "diaSemana": "Sábado"},
                    {"id": 7, "diaSemana": "Domingo"}
                ];
                $scope.diaSemana = $scope.dias[0];
                $scope.hora = new Date();
                $scope.hora.setHours(6);
                $scope.hora.setMinutes(30);
                $scope.hora.setSeconds(0);
                $scope.hora.setMilliseconds(0);
            }

            // Actions
            // -----------------------------------
            $scope.modalHorarioDistribucion = function () {
                $("#modalHorarioDistribucion").modal();
            };

            $scope.cambiarEstadoHorario = function (id) {
                var salir = false;
                for (var i = 0; i < $scope.horarios.length && !salir; i++) {
                    if ($scope.horarios[i].id == id) {
                        salir = true;
                        var params = {
                            csrfmiddlewaretoken: $("#myCsrfInput").val(),
                            idHorario: id
                        };

                        $scope.horarios[i].estado = !$scope.horarios[i].estado;
                        if ($scope.horarios[i].estado) {
                            alertify.success("Horario activado.");
                        } else {
                            alertify.success("Horario inactivado.");
                        }

                        $.post(Urls.rest_horario_cambiar_estado(), params, function (data) {
                            if (data.success) {

                            } else {
                                alertify.error("Error al cambiar el estado del horario");
                            }
                        });
                    }
                }
            };

            $scope.guardarHorarioDistribucion = function () {
                var error = false;
                var h = $scope.hora.getHours();
                var m = $scope.hora.getMinutes();
                var hour="";
                $scope.mensaje = "";
                if(h<10){
                    hour = "0"+h+":";
                }else{
                    hour = h+":";
                }

                if(m<10){
                    hour = hour+"0"+m+":00";
                }else{
                    hour = hour+m+":00";
                }
                //Se valida que no exista el horario
                for (var i = 0; i < $scope.horarios.length; i++) {
                    if ($scope.horarios[i].diaSemana == $scope.diaSemana.diaSemana
                        && $scope.horarios[i].hora == hour) {
                        $scope.mensaje = "Horario duplicado.";
                        error = true;
                    }
                }

                if (!error) {
                    var horario = {
                        csrfmiddlewaretoken: $("#myCsrfInput").val(),
                        "diaSemana": $scope.diaSemana.id,
                        "hora": hour
                    };
                    // Rest creación
                        $.post(Urls.rest_horario_crear(), horario, function (data) {
                            if (data.success) {
                                getListHorarios($scope.page);
                                alertify.success("Horario creado.");
                                getListHorarios($scope.page);
                            } else {
                                alertify.error("Error al cambiar el estado del horario");
                            }
                        });
                    $('#modalHorarioDistribucion').modal('toggle');
                }
            };
        }]);
})(angular);