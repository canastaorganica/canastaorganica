(function (angular) {
    var site = angular.module('site');

    site.controller('PedidosCtrl', ['$scope', '$state', '$stateParams', 'PedidosSvc',
        function ($scope, $state, $stateParams, PedidosSvc) {
            $scope.page = 1;
            if ($stateParams.page) {
                if ($stateParams.page < 1) {
                    $state.go("productor/pedido", {page: 1});
                    return;
                }
                $scope.page = $stateParams.page;
            }

            synchronizePromises();

            // Carga Inicial
            // -----------------------------------
            function synchronizePromises() {
                getListPedidos($scope.page).then(function () {
                    setTimeout(function () {

                    }, 0);
                });
            }

            function getListPedidos(page) {
                return PedidosSvc.list(page).then(function (data) {
                    if (page > data.num_pages) {
                        $state.go("productor/pedido", {page: data.num_pages});
                        return;
                    }
                    $scope.pedidos = data.pedidos;
                    $scope.num_siguiente = data.num_siguiente;
                    $scope.num_anterior = data.num_anterior;
                    $scope.num_pages = new Array(data.num_pages);
                    // Remove loading and display ordenes
                    $("#id_loading").hide();
                    $("#id_pedido_wrapper").show();
                });
            }
        }]);
})(angular);