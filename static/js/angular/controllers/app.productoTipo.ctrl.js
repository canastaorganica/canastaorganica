(function (angular) {
    var site = angular.module('site');

    site.controller('ProductoTipoCtrl', ['$scope', '$state', '$stateParams',
        function ($scope, $state, $stateParams) {
            $scope.page = 1;
            $scope.ofertas = [];
            $scope.activo = true;
            if ($stateParams.page) {
                if ($stateParams.page < 1) {
                    $state.go("admin/productoTipo", {page: 1});
                    return;
                }
                $scope.page = $stateParams.page;
            }

            synchronizePromises();

            // Carga Inicial
            // -----------------------------------
            function synchronizePromises() {
                $.get(Urls.rest_periodo_listar(), function (data) {
                    $scope.semanas = [];
                    data.forEach(function (obj) {
                        $scope.semanas.push({
                            id: obj.id,
                            fechaInicio: obj.fechaInicio,
                            fechaFin: obj.fechaFin,
                            label: "Desde " + obj.fechaInicio.substring(0, 10) + " hasta " + obj.fechaFin.substring(0, 10)
                        });
                    });

                    if ($scope.semanas.length > 0) {
                        $scope.semanaSlc = $scope.semanas[0];
                        cargarOfertas();
                    }
                });
            }

            function cargarOfertas() {
                var fecha = new Date($scope.semanaSlc.fechaInicio);
                fecha = new Date(fecha.valueOf() + fecha.getTimezoneOffset() * 60000);
                fechaAct = new Date();
                $scope.activo = fecha > fechaAct;
                //Consultar las ofertas de la semana
                $.get(Urls.rest_productos_tipo_periodo($scope.page, $scope.semanaSlc.id), function (data) {
                    if ($scope.page > data.num_pages) {
                        $state.go("admin/productoTipo", {page: data.num_pages});
                        return;
                    }

                    $scope.ofertas = data.ofertas;
                    $scope.num_siguiente = data.num_siguiente;
                    $scope.num_anterior = data.num_anterior;
                    $scope.num_pages = new Array(data.num_pages);
                    $scope.$apply();
                });
            }

            function crearDetalleCanasta(idCanasta, idProducto, cantidad) {
                var params = {
                    csrfmiddlewaretoken: $("#myCsrfInput").val(),
                    id_canasta: idCanasta,
                    id_producto: idProducto,
                    cantidad: cantidad
                };

                $.post(Urls.rest_productos_tipo_adicionar_detalle(), params, function (data) {
                    if (data.success) {
                        //alertify.success("OK");
                    } else {
                        alertify.error("Error al guardar el detalle");
                    }
                });
            }


            function modificarDetalleCanasta(idDetalle, cantidad) {
                var params = {
                    csrfmiddlewaretoken: $("#myCsrfInput").val(),
                    id_detalle: idDetalle,
                    cantidad: cantidad
                };

                $.post(Urls.rest_productos_tipo_modificar_detalle(), params, function (data) {
                    if (data.success) {
                        //alertify.success("OK");
                    } else {
                        alertify.error("Error al guardar el detalle");
                    }
                });
            }


            function eliminarDetalleCanasta(idDetalle) {
                var params = {
                    csrfmiddlewaretoken: $("#myCsrfInput").val(),
                    id_detalle: idDetalle
                };

                $.post(Urls.rest_productos_tipo_eliminar_detalle(), params, function (data) {
                    if (data.success) {
                        //alertify.success("OK");
                    } else {
                        alertify.error("Error al guardar el detalle");
                    }
                });
            }


            // Actions
            // -----------------------------------
            $scope.cambioValor = function (idOferta) {
                var valorVenta = 0;
                //Se busca el valor a guardar
                for (var i = 0; i < $scope.ofertas.length; i++) {
                    if ($scope.ofertas[i].id == idOferta) {
                        valorVenta = $scope.ofertas[i].valorVenta
                    }
                }

                var params = {
                    csrfmiddlewaretoken: $("#myCsrfInput").val(),
                    idOferta: idOferta,
                    valorVenta: valorVenta
                };

                $.post(Urls.rest_oferta_cambiar_valor(), params, function (data) {
                    if (data.success) {
                        //alertify.success("OK");
                    } else {
                        alertify.error("Error al guardar el valor");
                    }
                });
            };

            $scope.cambioSemana = function () {
                cargarOfertas();
            };

            $scope.modalProductosCanasta = function (idOferta) {
                //Consultar las ofertas de la semana
                $scope.ofertasModal = [];
                $.get(Urls.rest_productos_ofertados_periodo_all($scope.semanaSlc.id),
                    function (data) {
                        $scope.ofertasModal = data;

                        if (idOferta == 0) {
                            for (var i = 0; i < $scope.ofertasModal.length; i++) {
                                $scope.ofertasModal[i].estado = false;
                                $scope.ofertasModal[i].cantidad = 0;
                            }
                            $scope.$apply();
                        } else{


                            //Consultamos los productos de la canasta
                            $.get(Urls.rest_productos_producto_tipo(idOferta),
                                function (dataProd) {
                                    $scope.productosCanasta = dataProd;
                                    //Se carga la información en el modal
                                    for (var i = 0; i < $scope.ofertasModal.length; i++) {
                                        //Si es la misma oferta se quita de la lista
                                        if ($scope.ofertasModal[i].id == idOferta) {
                                            $scope.ofertasModal.splice(i, 1);
                                            i = i-1;
                                        } else {
                                            $scope.ofertasModal[i].estado = false;
                                            $scope.ofertasModal[i].cantidad = 0;

                                            for (var ct = 0; ct < $scope.productosCanasta.length; ct++) {
                                                if ($scope.ofertasModal[i].id == $scope.productosCanasta[ct].producto.id) {
                                                    $scope.ofertasModal[i].estado = true;
                                                    $scope.ofertasModal[i].cantidad = $scope.productosCanasta[ct].cantidad;
                                                }
                                            }

                                            //Se eliminan los productos que no estan activos para una semana que no se puede modificar
                                            if(!$scope.activo && !$scope.ofertasModal[i].estado){
                                                $scope.ofertasModal.splice(i, 1);
                                                i = i-1;
                                            }
                                        }
                                    }
                                    $scope.$apply();
                                });
                            }

                    }
                );
                $scope.productosEliminar = [];
                $scope.idOfertaModal = idOferta;
                $('#modalProductosCanasta').modal('toggle');
            }


            $scope.seleccionarOferta = function (idOferta) {
                for (var i = 0; i < $scope.ofertasModal.length; i++) {
                    if ($scope.ofertasModal[i].id == idOferta) {
                        $scope.ofertasModal[i].estado = !$scope.ofertasModal[i].estado;
                        break;
                    }
                }
            }


            $scope.eliminarProductoItem = function (idOferta) {
                var params = {
                    csrfmiddlewaretoken: $("#myCsrfInput").val(),
                    id_oferta: idOferta
                };

                $.post(Urls.rest_productos_tipo_eliminar(), params, function (data) {
                    if (data.success) {
                        alertify.success("Canasta eliminada correctamente.");
                        cargarOfertas();
                    } else {
                        alertify.error("Error al eliminar la canasta.");
                    }
                });
            }


            $scope.guardarCanasta = function () {
                var error = false;
                $scope.mensaje = "";
                for (var i = 0; i < $scope.ofertasModal.length && !error; i++) {
                    if ($scope.ofertasModal[i].estado && $scope.ofertasModal[i].cantidad <= 0) {
                        error = true;
                        $scope.mensaje ="La cantidad de productos no puede ser menor o igual a cero.";
                    }
                }

                //Proceso para guardar los productos
                if (!error) {
                    if ($scope.idOfertaModal == 0) {
                        var params = {
                            csrfmiddlewaretoken: $("#myCsrfInput").val(),
                            id_periodo: $scope.semanaSlc.id,
                            valor_venta: 0
                        };
                        $.post(Urls.rest_productos_tipo_crear(), params, function (data) {
                            if (data > 0) {
                                for (var i = 0; i < $scope.ofertasModal.length; i++) {
                                    if($scope.ofertasModal[i].estado != undefined && $scope.ofertasModal[i].estado){
                                        crearDetalleCanasta(data, $scope.ofertasModal[i].id, $scope.ofertasModal[i].cantidad);
                                    }
                                }
                                cargarOfertas();
                            } else {
                                alertify.error("Error al guardar el detalle");
                            }
                        });
                    } else {
                        for (var i = 0; i < $scope.ofertasModal.length; i++) {
                            //canasta modificada
                            if (!$scope.ofertasModal[i].estado) {
                                //Se valida si fue eliminado
                                for (var ii=0; ii <$scope.productosCanasta.length; ii++) {
                                    if ($scope.productosCanasta[ii].producto.id == $scope.ofertasModal[i].id) {
                                        //ejecutar la rutina para eliminar
                                        eliminarDetalleCanasta($scope.productosCanasta[ii].id);
                                        break;
                                    }
                                }
                            } else {
                                //Se valida si fue creado o fue modificado
                                var isModificado = false;
                                for (var ii=0; ii < $scope.productosCanasta.length; ii++) {
                                    if ($scope.productosCanasta[ii].producto.id == $scope.ofertasModal[i].id) {
                                        isModificado = true;
                                        if ($scope.productosCanasta[ii].cantidad != $scope.ofertasModal[i].cantidad) {
                                            //Ejecutar la rutina para actualizar detalle de canasta
                                            modificarDetalleCanasta($scope.productosCanasta[ii].id, $scope.ofertasModal[i].cantidad);
                                        }
                                        break;
                                    }
                                }

                                if (!isModificado) {
                                    //ejecuta la rutina para adicionar detalle a la canasta
                                    crearDetalleCanasta($scope.idOfertaModal, $scope.ofertasModal[i].id, $scope.ofertasModal[i].cantidad);
                                }
                            }

                        }
                    }
                    $('#modalProductosCanasta').modal('toggle');
                    alertify.success("Información almacenada correctamente.");
                }
            }
        }]);
})(angular);