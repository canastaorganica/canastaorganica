(function (angular) {
    var site = angular.module('site');

    site.controller('RegistroClienteCtrl', ['$scope', '$state', '$stateParams', 'UsersSvc',
        function ($scope, $state, $stateParams, UsersSvc) {
            var idUsuario;
            if ($stateParams.idUsuario) {
                idUsuario = $stateParams.idUsuario;
            } else {
                idUsuario = 0;
            }

            synchronizePromises();

            // Carga Inicial
            // -----------------------------------
            function synchronizePromises() {
                getForm().then(function () {

                });
            }

            function getForm() {
                return UsersSvc.clienteForm(idUsuario).then(function (data) {
                    $("#form_wrapper").html(data.user_form + data.cliente_form);
                });
            }

            // Actions
            // -----------------------------------
            $scope.registrar = function () {
                $('button[type="submit"]').prop('disabled', true);
                $("#csrfmiddlewaretoken").val($("#myCsrfInput").val());
                var url = Urls.rest_users_cliente_form(0) + "0";
                if (idUsuario > 0) {
                    url = Urls.rest_users_cliente_form(idUsuario);
                }
                $.post(url, $('#form').serialize(),
                    function (data) {
                        // Verifica que venga el mensaje
                        if (!data.mensaje) {
                            data.mensaje = "Error";
                        }
                        // Muestra mensaje de acuerdo al tipo
                        if (data.success) {
                            $scope.getActualUser().then(function () {
                                $state.go("catalogo", {page: 1, certificado: 0}, {reload: true});
                            });
                            if (idUsuario > 0) {
                                alertify.success(data.mensaje);
                            } else {
                                $scope.getNumItems();
                                alertify.success("Bienvenido!");
                            }
                        } else {
                            alertify.error(data.mensaje);
                            $("#form_wrapper").html(data.user_form + data.cliente_form);
                            $('html, body').animate({scrollTop: 0}, 'slow');
                        }
                        $('button[type="submit"]').prop('disabled', false);
                    });
            };
        }]);

    site.controller('RegistroProductorCtrl', ['$scope', '$state', '$stateParams', 'UsersSvc',
        function ($scope, $state, $stateParams, UsersSvc) {
            var idUsuario;
            if ($stateParams.idUsuario) {
                idUsuario = $stateParams.idUsuario;
            } else {
                idUsuario = 0;
            }

            synchronizePromises();

            // Carga Inicial
            // -----------------------------------
            function synchronizePromises() {
                getForm().then(function () {

                });
            }

            function getForm() {
                return UsersSvc.productorForm(idUsuario).then(function (data) {
                    $("#form_wrapper").html(data.user_form + data.productor_form);
                });
            }

            // Actions
            // -----------------------------------
            $scope.registrar = function () {
                $('button[type="submit"]').prop('disabled', true);
                $("#csrfmiddlewaretoken").val($("#myCsrfInput").val());
                var url = Urls.rest_users_productor_form(0) + "0";
                if (idUsuario > 0) {
                    url = Urls.rest_users_productor_form(idUsuario);
                }
                $.post(url, $('#form').serialize(),
                    function (data) {
                        // Verifica que venga el mensaje
                        if (!data.mensaje) {
                            data.mensaje = "Error";
                        }
                        // Muestra mensaje de acuerdo al tipo
                        if (data.success) {
                            $scope.getActualUser().then(function () {
                                $scope.redireccionPorGrupo();
                            });
                            if (idUsuario > 0) {
                                alertify.success(data.mensaje);
                            } else {
                                $scope.getNumItems();
                                alertify.success("Bienvenido!");
                            }
                        } else {
                            alertify.error(data.mensaje);
                            $("#form_wrapper").html(data.user_form + data.productor_form);
                            $('html, body').animate({scrollTop: 0}, 'slow');
                        }
                        $('button[type="submit"]').prop('disabled', false);
                    });
            };
        }]);
})(angular);