(function (angular) {
    var site = angular.module('site');

    site.controller('CanastaCtrl', ['$scope', '$state', '$stateParams', 'CanastaSvc',
        function ($scope, $state, $stateParams, CanastaSvc) {
            $scope.page = 1;
            if ($stateParams.page) {
                if ($stateParams.page < 1) {
                    $state.go("canasta", {page: 1});
                    return;
                }
                $scope.page = $stateParams.page;
            }

            synchronizePromises();

            // Carga Inicial
            // -----------------------------------
            function synchronizePromises() {
                getListProductos($scope.page).then(function () {

                });
                getHorarios().then(function () {

                });
                getValorItems().then(function () {
                    $scope.valor_envio = 0;
                });
            }

            function getListProductos(page) {
                return CanastaSvc.list(page).then(function (data) {
                    if (page > data.num_pages) {
                        $state.go("canasta", {page: data.num_pages});
                        return;
                    }
                    $scope.items = data.items;
                    $scope.user_athenticated = data.user_athenticated;
                    $scope.num_siguiente = data.num_siguiente;
                    $scope.num_anterior = data.num_anterior;
                    $scope.num_pages = new Array(data.num_pages);
                    // Remove loading and display canasta
                    $("#id_loading").hide();
                    $("#id_canasta_wrapper").show();
                });
            }

            function getValorItems() {
                return CanastaSvc.getValorItems().then(function (data) {
                    $scope.valor_items = data.valor_items;
                });
            }

            function getHorarios() {
                return CanastaSvc.horarios().then(function (data) {
                    $scope.horarios = [];
                    data.horarios.forEach(function (obj) {
                        $scope.horarios.push({
                            id: obj.id,
                            diaSemana: obj.diaSemana,
                            hora: obj.hora,
                            label: obj.diaSemana + " - " + obj.hora
                        });
                    });
                    $scope.horario_distribucion = $scope.horarios[0];
                });
            }

            // Actions
            // -----------------------------------
            $scope.eliminarItem = function (idItem) {
                var params = {
                    csrfmiddlewaretoken: $("#myCsrfInput").val(),
                    idItemCarro: idItem
                };
                $.post(Urls.rest_carro_items_eliminar(), params, function (data) {
                    if (data.success) {
                        alertify.success("Producto eliminado del carro");
                        $scope.getNumItems();
                        $state.go($state.current, {}, {reload: true});
                    } else {
                        alertify.error("Error al eliminar el producto del carro");
                    }
                });
            };

            $scope.modalHorarioDistribucion = function () {
                $("#modalHorarioDistribucion").modal();
            };

            $scope.cambiarHorarioDistribucion = function () {
                $('#modalHorarioDistribucion').modal('toggle');
            };

            $scope.pagar = function () {
                $scope.itemsNoDisponibles = [];
                $.get(Urls.rest_carro_items_no_disponibles(), function (data) {
                    $scope.itemsNoDisponibles = data;

                    if ($scope.itemsNoDisponibles.length == 0) {
                        var params = {
                            csrfmiddlewaretoken: $("#myCsrfInput").val(),
                            id_horario: $scope.horario_distribucion.id
                        };
                        $.post(Urls.rest_carro_horarios_asignar(), params, function (data) {
                            if (data.success) {
                                var params = {
                                    csrfmiddlewaretoken: $("#myCsrfInput").val(),
                                    estado: 'PEN'
                                };
                                $.post(Urls.rest_carro_estado_actualizar(), params, function (data) {
                                    if (data.success) {
                                        app_redirect(Urls.pagar());
                                    } else {
                                        alertify.error("Error al actualizar estado de la carro");
                                    }
                                });
                            } else {
                                alertify.error("Error al asignar el Horario de Distribución");
                            }
                        });
                    } else{
                        $("#modalItemsNoDisponibles").modal();
                        $scope.$apply();
                    }
                });
            };

            $scope.modalCancelarPago = function () {
                $('#modalItemsNoDisponibles').modal('toggle');
            };

            $scope.modalAceptarPago = function () {
                for(var i=0; i < $scope.itemsNoDisponibles.length; i++){
                    $scope.eliminarItem($scope.itemsNoDisponibles[i].id);
                }
                $scope.pagar();
            };
        }]);
})(angular);