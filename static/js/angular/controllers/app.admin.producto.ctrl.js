/**
 * Created by JuanDavid on 17/04/2016.
 */
(function (angular) {
    var site = angular.module('site');

    site.controller('AdminProductosCtrl', ['$scope', '$state', '$stateParams', 'AdminProductosSvc', 'TiposSvc',
        function ($scope, $state, $stateParams, AdminProductosSvc, TiposSvc) {
            $scope.page = 1;
            if ($stateParams.page) {
                if ($stateParams.page < 1) {
                    $state.go("admin/producto", {page: 1});
                    return;
                }
                $scope.page = $stateParams.page;
            }

            synchronizePromises();

            // Carga Inicial
            // -----------------------------------
            function synchronizePromises() {
                getListaUnidades().then(function () {
                    setTimeout(function () {
                    }, 0);
                });
                getListaProductos($scope.page).then(function () {
                    setTimeout(function () {
                    }, 0);
                });
                getForm().then(function () {
                    setTimeout(function () {
                    }, 0);
                });
            }

            /**
             * Consulta la lista de productos paginada
             * @param page
             * @returns {*}
             */
            function getListaProductos(page) {
                return AdminProductosSvc.list(page).then(function (data) {
                    if (page > data.num_pages) {
                        $state.go("admin/producto", {page: data.num_pages});
                        return;
                    }
                    $scope.productos = data.productos;
                    $scope.num_siguiente = data.num_siguiente;
                    $scope.num_anterior = data.num_anterior;
                    $scope.num_pages = new Array(data.num_pages);
                    // Remove loading and display ordenes
                    $("#id_loading").hide();
                    $("#id_producto_wrapper").show();
                });
            }

            /**
             * Consulta la lista de tipo de unidades
             * @returns {*}
             */
            function getListaUnidades() {
                return TiposSvc.unidades().then(function (data) {
                    $scope.unidades = data.tipos_unidades;
                    $scope.unidad = $scope.unidades[0];
                });
            }


            function getForm() {
                return AdminProductosSvc.form().then(function (data) {
                    $("#form_wrapper").html(data.form);
                });
            }

            // -----------------------------------
            // Actions
            // -----------------------------------
            $scope.abrirModalNuevoProducto = function () {
                $("#modalNuevoProducto").modal();
            };

            $scope.guardarProducto = function () {
                $('button[type="submit"]').prop('disabled', true);
                $("#csrfmiddlewaretoken").val($("#myCsrfInput").val());
                // FORM DATA
                var formData = new FormData($('#form')[0]);
                // File
                var file = $('#id_foto').get(0).files[0];
                formData.append('foto', file);
                // REQUEST
                $.ajax({
                    url: Urls.rest_producto_crear_form(),
                    data: formData,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function (data) {
                        // Verifica que venga el mensaje
                        if (!data.mensaje) {
                            data.mensaje = "Error";
                        }
                        // Muestra mensaje de acuerdo al tipo
                        if (data.success) {
                            $('#modalNuevoProducto').modal('hide');
                            setTimeout(function () {
                                $state.go($state.current, {page: 1}, {reload: true});
                            }, 250);
                            alertify.success(data.mensaje);
                        } else {
                            alertify.error(data.mensaje);
                            $("#form_wrapper").html(data.form);
                        }
                        $('button[type="submit"]').prop('disabled', false);
                    }
                });
            };

        }]);
})(angular);