/**
 * Created by JuanDavid on 17/04/2016.
 */
(function (angular) {
    var site = angular.module('site');

    site.controller('OrdenesCtrl', ['$scope', '$state', '$stateParams', 'OrdenesSvc',
        function ($scope, $state, $stateParams, OrdenesSvc) {
            $scope.page = 1;
            if ($stateParams.page) {
                if ($stateParams.page < 1) {
                    $state.go("admin/orden", {page: 1});
                    return;
                }
                $scope.page = $stateParams.page;
            }

            synchronizePromises();

            // Carga Inicial
            // -----------------------------------
            function synchronizePromises() {
                getListOrdenes($scope.page).then(function () {
                    
                });
            }

            function getListOrdenes(page) {
                return OrdenesSvc.list(page).then(function (data) {
                    if (page > data.num_pages) {
                        $state.go("admin/orden", {page: data.num_pages});
                        return;
                    }
                    $scope.ordenes = data.ordenes;
                    $scope.num_siguiente = data.num_siguiente;
                    $scope.num_anterior = data.num_anterior;
                    $scope.num_pages = new Array(data.num_pages);
                    // Remove loading and display ordenes
                    $("#id_loading").hide();
                    $("#id_catalogo_wrapper").show();
                });
            }
        }]);
})(angular);