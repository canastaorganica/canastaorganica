/**
 * Created by JuanDavid on 17/04/2016.
 */
(function (angular) {
    var site = angular.module('site');

    site.controller('AdminNotificarCtrl', ['$scope', '$state',
        function ($scope, $state) {
            synchronizePromises();

            // Carga Inicial
            // -----------------------------------
            function synchronizePromises() {

            }

            // -----------------------------------
            // Actions
            // -----------------------------------
            $scope.enviarCorreos = function () {
                swal({
                    title: "¿Enviar Correos?",
                    text: "Se enviará un correo a cada consumidor notificando la disponibilidad de los productos en su lista de deseos",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sí, enviar!",
                    closeOnConfirm: true
                }, function () {
                    $.get(Urls.enviar_correos_lista_deseos(), {}, function (data) {
                        if (data.success) {
                            swal("Enviados!", "Se enviaron los correos", "success");
                        } else {
                            swal("Error!", "Error al enviar correos", "error");
                        }
                    });
                });
            };
        }]);
})(angular);