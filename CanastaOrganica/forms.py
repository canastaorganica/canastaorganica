# coding=utf-8
from django import forms
from django.contrib.auth import password_validation
from django.contrib.auth.models import User
from django.forms import ModelForm
from django.utils.translation import ugettext as _

from CanastaOrganica.models import TransaccionPasarela, Cliente, Productor, Producto


# Se extiende la clase del módulo auth
class UserCreationForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and
    password.
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }
    password1 = forms.CharField(label=_("Password"),
                                widget=forms.PasswordInput)
    password2 = forms.CharField(label=_("Password confirmation"),
                                widget=forms.PasswordInput,
                                help_text=_("Enter the same password as before, for verification."))

    class Meta:
        model = User
        fields = ("username", "first_name", "last_name", "email")

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        self.instance.username = self.cleaned_data.get('username')
        password_validation.validate_password(self.cleaned_data.get('password2'), self.instance)
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs \
            .update({
            'placeholder': 'Usuario',
            'class': 'form-control'
        })
        self.fields['password1'].widget.attrs \
            .update({
            'placeholder': 'Contraseña',
            'class': 'form-control'
        })
        self.fields['password2'].widget.attrs \
            .update({
            'placeholder': 'Confirme Contraseña',
            'class': 'form-control'
        })
        self.fields['first_name'].widget.attrs \
            .update({
            'placeholder': 'Nombres del Usuario',
            'class': 'form-control'
        })
        self.fields['last_name'].widget.attrs \
            .update({
            'placeholder': 'Apellidos del Usuario',
            'class': 'form-control'
        })
        self.fields['email'].widget.attrs \
            .update({
            'placeholder': 'Email del Usuario',
            'class': 'form-control'
        })


class ClienteForm(forms.ModelForm):
    class Meta:
        model = Cliente
        exclude = ['user']

    def save(self, commit=True):
        super(ClienteForm, self).save(commit=commit)

    def __init__(self, *args, **kwargs):
        super(ClienteForm, self).__init__(*args, **kwargs)
        self.fields['direccion'].label = "Dirección"
        self.fields['direccion'].widget.attrs \
            .update({
            'placeholder': 'Dirección del Usuario',
            'class': 'form-control'
        })
        self.fields['telefono'].label = "Teléfono"
        self.fields['telefono'].widget.attrs \
            .update({
            'placeholder': 'Teléfono del Usuario',
            'class': 'form-control'
        })


class ProductorForm(forms.ModelForm):
    class Meta:
        model = Productor
        exclude = ['user', 'certificado', 'activo']

    def save(self, commit=True):
        super(ProductorForm, self).save(commit=commit)

    def __init__(self, *args, **kwargs):
        super(ProductorForm, self).__init__(*args, **kwargs)
        self.fields['telefono'].label = "Teléfono"
        self.fields['telefono'].widget.attrs \
            .update({
            'placeholder': 'Teléfono del Usuario',
            'class': 'form-control'
        })


class TransaccionPasarelaForm(ModelForm):
    referenciaPago = forms.CharField(widget=forms.HiddenInput)
    valorTx = forms.CharField(widget=forms.HiddenInput)
    fechaTx = forms.CharField(widget=forms.HiddenInput)

    class Meta:
        model = TransaccionPasarela
        exclude = ['estado']

    def save(self, commit=True):
        super(TransaccionPasarelaForm, self).save(commit=commit)


class ProductoForm(forms.ModelForm):
    class Meta:
        model = Producto
        fields = ("nombre", "descripcion", "foto", "tipoUnidad")

    def save(self, commit=True):
        super(ProductoForm, self).save(commit=commit)

    def __init__(self, *args, **kwargs):
        super(ProductoForm, self).__init__(*args, **kwargs)
        self.fields['descripcion'].label = "Descripción"
        self.fields['tipoUnidad'].label = "Unidad"

        self.fields['nombre'].widget.attrs \
            .update({
            'placeholder': 'Nombre del producto',
            'class': 'form-control'
        })

        self.fields['descripcion'].widget.attrs \
            .update({
            'placeholder': 'Descripción del producto',
            'class': 'form-control'
        })

        self.fields['foto'].widget.attrs \
            .update({
            'class': 'form-control'
        })

        self.fields['tipoUnidad'].widget.attrs \
            .update({
            'class': 'form-control'
        })
