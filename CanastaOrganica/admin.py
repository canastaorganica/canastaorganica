from django.contrib import admin

from CanastaOrganica.models import Producto, Productor, OfertaProductor, OfertaProducto, HorarioDistribucion

# Register your models here.
admin.site.register(Producto)
admin.site.register(Productor)
admin.site.register(OfertaProducto)
admin.site.register(OfertaProductor)
admin.site.register(HorarioDistribucion)
