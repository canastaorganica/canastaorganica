# coding=utf-8
import datetime
import json

from django import middleware
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password
from django.core.mail import EmailMessage
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.utils.timezone import now

from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser

from CanastaOrganica.forms import TransaccionPasarelaForm, ClienteForm, UserCreationForm, ProductorForm, ProductoForm
from CanastaOrganica.models import Carro, ItemCarro, OfertaProducto, PeriodoCompra, \
    OfertaProductor, HorarioDistribucion, Cliente, Productor, Producto, TipoProducto, DetalleCanasta, \
    ProductosFavoritos, TipoUnidad, Solicitudes
from CanastaOrganica.serializers import OfertaProductoSerializer, ItemCarroSerializer, UserSimpleSerializer, \
    HorarioDistribucionSerializer, PeriodoCompraSerializer, OfertaProductorSerializer, \
    HorarioDistribucionPaginadoSerializer, CarroSerializer, DetalleCanastaSerializer, ProductoSerializer, \
    TipoUnidadSerializer, ProductorSerializer


# ===========================================================================
# MAIN VIEWS
# ===========================================================================
def index(request):
    if "ID_CANASTA" not in request.session:
        request.session["ID_CANASTA"] = 0
    return render(request, 'base_public_layout.html')


# ===========================================================================
# PRODUCTO VIEWS
# ===========================================================================
def productos_unidades(id_oferta):
    unidades_disponibles = 0
    fecha_actual = datetime.datetime.now()
    periodo_actual = PeriodoCompra.objects.filter(fechaInicio__lte=fecha_actual, fechaFin__gte=fecha_actual)
    oferta = OfertaProducto.objects.get(id=id_oferta)
    producto = Producto.objects.get(id=oferta.producto.id)
    if producto.tipoProducto.id == 1:
        ofertas_productor = OfertaProductor.objects.filter(producto=oferta.producto.id, periodo=periodo_actual.last())
        for oferta in ofertas_productor:
            unidades_disponibles += (oferta.cantidadCompra - oferta.cantidadVendida)
    else:
        detalle_producto = DetalleCanasta.objects.filter(canasta=id_oferta)
        unidades_disponibles = -1
        for producto in detalle_producto:
            cantidad = int(productos_unidades(producto.producto.id) / producto.cantidad)
            if cantidad < unidades_disponibles or unidades_disponibles == -1:
                unidades_disponibles = cantidad
        if unidades_disponibles == -1:
            unidades_disponibles = 0

    return unidades_disponibles


def rest_productos_unidades(request, id_oferta):
    data = {
        "unidades_disponibles": productos_unidades(id_oferta)
    }
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_productos_ofertados(request, page, certificado):
    # Objetos
    fecha_actual = datetime.datetime.now()
    periodo_actual = PeriodoCompra.objects.filter(fechaInicio__lte=fecha_actual, fechaFin__gte=fecha_actual)
    if certificado:
        productores_certificados = Productor.objects.filter(certificado=True)
        productos_certificados = OfertaProductor.objects.filter(productor__in=productores_certificados).values_list(
            'producto', flat=True)
        ofertas = OfertaProducto.objects.filter(periodo=periodo_actual.last(), producto__in=productos_certificados)
    else:
        ofertas = OfertaProducto.objects.filter(periodo=periodo_actual.last())
    # Paginación
    paginator = Paginator(ofertas, 6)
    try:
        ofertas = paginator.page(page)
    except (InvalidPage, EmptyPage):
        ofertas = paginator.page(paginator.num_pages)
    if ofertas.has_previous():
        num_anterior = ofertas.number - 1
    else:
        num_anterior = ofertas.number
    if ofertas.has_next():
        num_siguiente = ofertas.number + 1
    else:
        num_siguiente = ofertas.number

    user_athenticated = False
    if "_auth_user_id" in request.session:
        user_athenticated = True
    # JSON
    data_obj = OfertaProductoSerializer(ofertas.object_list, many=True, context={'request': request}).data
    data = {
        "ofertas": data_obj,
        "num_anterior": num_anterior,
        "num_siguiente": num_siguiente,
        "num_pages": ofertas.paginator.num_pages,
        "user_athenticated": user_athenticated
    }
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_productos_ofertas(request, page):
    return rest_productos_ofertados(request, page, False)


def rest_productos_certificados(request, page):
    return rest_productos_ofertados(request, page, True)


def rest_productos_ofertados_periodo(request, page, id_periodo):
    # Objetos
    periodo_actual = PeriodoCompra.objects.get(id=id_periodo)
    tipoProducto = TipoProducto.objects.get(id=1)
    ofertas = OfertaProducto.objects.filter(periodo=periodo_actual, producto__tipoProducto=tipoProducto)
    # Paginación
    paginator = Paginator(ofertas, 12)
    try:
        ofertas = paginator.page(page)
    except (InvalidPage, EmptyPage):
        ofertas = paginator.page(paginator.num_pages)
    if ofertas.has_previous():
        num_anterior = ofertas.number - 1
    else:
        num_anterior = ofertas.number
    if ofertas.has_next():
        num_siguiente = ofertas.number + 1
    else:
        num_siguiente = ofertas.number
    # JSON
    data_obj = OfertaProductoSerializer(ofertas.object_list, many=True).data
    data = {
        "ofertas": data_obj,
        "num_anterior": num_anterior,
        "num_siguiente": num_siguiente,
        "num_pages": ofertas.paginator.num_pages
    }
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_productos_ofertados_periodo_all(request, id_periodo):
    # Objetos
    periodo_actual = PeriodoCompra.objects.get(id=id_periodo)
    ofertas = OfertaProducto.objects.filter(periodo=periodo_actual)
    # JSON
    data = OfertaProductoSerializer(ofertas, many=True).data

    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


# ===========================================================================
# PRODUCTO CANASTA
# ===========================================================================
def rest_productos_tipo_periodo(request, page, id_periodo):
    tipoProducto = TipoProducto.objects.get(id=2)
    productoTipo = Producto.objects.get(tipoProducto=tipoProducto)
    periodo_actual = PeriodoCompra.objects.get(id=id_periodo)
    ofertas = OfertaProducto.objects.filter(periodo=periodo_actual, producto=productoTipo)
    # Paginación
    paginator = Paginator(ofertas, 12)
    try:
        ofertas = paginator.page(page)
    except (InvalidPage, EmptyPage):
        ofertas = paginator.page(paginator.num_pages)
    if ofertas.has_previous():
        num_anterior = ofertas.number - 1
    else:
        num_anterior = ofertas.number
    if ofertas.has_next():
        num_siguiente = ofertas.number + 1
    else:
        num_siguiente = ofertas.number
    # JSON
    data_obj = OfertaProductoSerializer(ofertas.object_list, many=True).data
    data = {
        "ofertas": data_obj,
        "num_anterior": num_anterior,
        "num_siguiente": num_siguiente,
        "num_pages": ofertas.paginator.num_pages
    }
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_productos_producto_tipo(request, id_canasta):
    # Objetos
    canasta = OfertaProducto.objects.get(id=id_canasta)
    detalleProductos = DetalleCanasta.objects.filter(canasta=canasta)
    # JSON
    data = DetalleCanastaSerializer(detalleProductos, many=True).data

    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_productos_tipo_crear(request):
    data = {}
    if request.method == "POST":
        id_periodo = int(request.POST['id_periodo'])
        valorVenta = float(request.POST['valor_venta'])

        tipoProducto = TipoProducto.objects.get(id=2)
        producto = Producto.objects.get(tipoProducto=tipoProducto)
        periodo = PeriodoCompra.objects.get(id=id_periodo)

        ofertaProducto = OfertaProducto.objects.create(valorVenta=valorVenta, periodo=periodo, producto=producto)
        ofertaProducto.save()
        data = ofertaProducto.id
    else:
        data = 0

    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_productos_tipo_eliminar(request):
    data = {}
    if request.method == "POST":
        id_oferta = int(request.POST['id_oferta'])
        oferta = OfertaProducto.objects.get(id=id_oferta)
        oferta.delete()
        data["success"] = True
    else:
        data["success"] = False

    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_productos_tipo_adicionar_detalle(request):
    data = {}
    if request.method == "POST":
        id_canasta = int(request.POST['id_canasta'])
        canasta = OfertaProducto.objects.get(id=id_canasta)
        id_producto = int(request.POST['id_producto'])
        producto = OfertaProducto.objects.get(id=id_producto)
        cantidad = int(request.POST['cantidad'])

        detalle = DetalleCanasta.objects.create(canasta=canasta, producto=producto, cantidad=cantidad)
        detalle.save()
        data["success"] = True
    else:
        data["success"] = False

    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_productos_tipo_modificar_detalle(request):
    data = {}
    if request.method == "POST":
        id_detalle = int(request.POST['id_detalle'])
        cantidad = int(request.POST['cantidad'])

        detalle = DetalleCanasta.objects.get(id=id_detalle)
        detalle.cantidad = cantidad
        detalle.save()
        data["success"] = True
    else:
        data["success"] = False

    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_productos_tipo_eliminar_detalle(request):
    data = {}
    if request.method == "POST":
        id_detalle = int(request.POST['id_detalle'])
        detalle = DetalleCanasta.objects.get(id=id_detalle)
        detalle.delete()
        data["success"] = True
    else:
        data["success"] = False

    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


# ===========================================================================
# PERIODOCOMPRA VIEWS
# ===========================================================================
def rest_periodo_listar(request):
    # Objetos
    periodos = PeriodoCompra.objects.all().order_by('-fechaInicio')[:5]
    periodos = PeriodoCompraSerializer(periodos, many=True).data
    return HttpResponse(json.dumps(periodos), content_type='application/json; charset=UTF-8')


# ===========================================================================
# OFERTAPRODUCTO VIEWS
# ===========================================================================
def rest_oferta_cambiar_valor(request):
    data = {}
    if request.method == "POST":
        # Se obtiene los valores del mensaje
        id_oferta = int(request.POST['idOferta'])
        valor = float(request.POST['valorVenta'])
        oferta_producto = OfertaProducto.objects.get(id=id_oferta)
        oferta_producto.valorVenta = valor
        oferta_producto.save()
        data["success"] = True
    else:
        data["success"] = False
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


# ===========================================================================
# OFERTAPRODUCTOR VIEWS
# ===========================================================================
def rest_producto_oferta_productores(request, id_periodo, id_producto):
    oferta = OfertaProductor.objects.filter(periodo_id=id_periodo, producto_id=id_producto)
    oferta = OfertaProductorSerializer(oferta, many=True).data
    return HttpResponse(json.dumps(oferta), content_type='application/json; charset=UTF-8')


# ===========================================================================
# CARRO VIEWS
# ===========================================================================
def adicionar_item(carro, producto, cantidad):
    try:
        item = ItemCarro.objects.get(producto=producto, carro=carro)
        item.cantidad += cantidad
        item.save()
    except ItemCarro.DoesNotExist:
        ItemCarro.objects.create(producto=producto, cantidad=cantidad, carro=carro)
    carro.save()


def rest_carro_items_count(request):
    num_productos = 0
    id_carro = request.session.get("ID_CANASTA", 0)
    if id_carro > 0:
        num_productos = ItemCarro.objects.filter(carro=id_carro).count()
    data = {
        "num_productos": num_productos
    }
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_carro_items_valor(request):
    id_carro = request.session.get("ID_CANASTA", 0)
    valor_total = 0
    if id_carro > 0:
        valor_total = Carro.objects.get(pk=id_carro).valor_total()
    data = {
        "valor_items": valor_total
    }
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_carro_horarios(request):
    data = {
        "horarios": HorarioDistribucionSerializer(
            HorarioDistribucion.objects.filter(estado=True).order_by('diaSemana', 'hora'), many=True).data
    }
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_carro_horarios_asignar(request):
    data = {
        "success": False
    }
    if request.method == "POST":
        id_horario = request.POST.get('id_horario', 0)
        if id_horario > 0:
            canasta_obj = Carro.objects.get(pk=request.session["ID_CANASTA"])
            canasta_obj.horario_distribucion_id = id_horario
            canasta_obj.save()
            data = {
                "success": True
            }
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def carro_estado_actualizar(id_carro, nuevo_estado):
    canasta_obj = Carro.objects.get(pk=id_carro)
    canasta_obj.estado = nuevo_estado
    canasta_obj.save()


def rest_carro_estado_actualizar(request):
    data = {
        "success": False
    }
    if request.method == "POST":
        id_carro = request.session["ID_CANASTA"]
        estado = request.POST.get('estado', '')
        if id_carro > 0 and estado != '':
            carro_estado_actualizar(id_carro, estado)
            data = {
                "success": True
            }
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_carro_agregar(request):
    data = {}
    if request.method == "POST":
        if request.session["ID_CANASTA"] == 0:
            if "_auth_user_id" in request.session:
                carro = Carro.objects.create(user=request.user)
            else:
                carro = Carro.objects.create()
            request.session["ID_CANASTA"] = carro.id
        else:
            carro = Carro.objects.get(id=request.session["ID_CANASTA"])
        # Se crea el item a la canasta
        id_producto = int(request.POST['idProducto'])
        cantidad = float(request.POST['cantidad'])
        producto = OfertaProducto.objects.get(id=id_producto)
        adicionar_item(carro, producto, cantidad)
        data["success"] = True
    else:
        data["success"] = False
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_carro_items(request, page):
    # Objetos
    id_canasta = request.session["ID_CANASTA"]
    items = ItemCarro.objects.filter(carro=id_canasta)
    # Paginación
    paginator = Paginator(items, 4)
    try:
        items = paginator.page(page)
    except (InvalidPage, EmptyPage):
        items = paginator.page(paginator.num_pages)
    if items.has_previous():
        num_anterior = items.number - 1
    else:
        num_anterior = items.number
    if items.has_next():
        num_siguiente = items.number + 1
    else:
        num_siguiente = items.number
    user_athenticated = False
    if "_auth_user_id" in request.session:
        user_athenticated = True
    # JSON
    data_obj = ItemCarroSerializer(items.object_list, many=True).data
    data = {
        "items": data_obj,
        "num_anterior": num_anterior,
        "num_siguiente": num_siguiente,
        "num_pages": items.paginator.num_pages,
        "user_athenticated": user_athenticated
    }
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_carro_items_eliminar(request):
    data = {}
    if request.method == "POST":
        try:
            id_item_carro = int(request.POST['idItemCarro'])
            item = ItemCarro.objects.get(id=id_item_carro)
            carro = item.carro
            item.delete()
            carro.save()
            data["success"] = True
        except:
            data["success"] = False
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_carro_items_no_disponibles(request):
    # Objetos
    itemsNoDisponibles = []
    id_canasta = request.session["ID_CANASTA"]
    items = ItemCarro.objects.filter(carro=id_canasta)

    for item in items:
        if (productos_unidades(item.producto.id) < item.cantidad):
            itemsNoDisponibles.append(item)
    itemsNoDisponibles = ItemCarroSerializer(itemsNoDisponibles, many=True).data

    return HttpResponse(json.dumps(itemsNoDisponibles), content_type='application/json; charset=UTF-8')


def rest_orden_listar(request, page):
    # Objetos
    ordenes = Carro.objects.exclude(estado="CAN").order_by('-fechaCreacion', '-id')

    # Paginación
    paginator = Paginator(ordenes, 10)
    try:
        ordenes = paginator.page(page)
    except (InvalidPage, EmptyPage):
        ordenes = paginator.page(paginator.num_pages)
    if ordenes.has_previous():
        num_anterior = ordenes.number - 1
    else:
        num_anterior = ordenes.number
    if ordenes.has_next():
        num_siguiente = ordenes.number + 1
    else:
        num_siguiente = ordenes.number

    # JSON
    data_obj = CarroSerializer(ordenes.object_list, many=True).data

    data = {
        "ordenes": data_obj,
        "num_anterior": num_anterior,
        "num_siguiente": num_siguiente,
        "num_pages": ordenes.paginator.num_pages
    }
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_pedido_productor(request, page):
    # Objetos
    data = {}

    if "_auth_user_id" in request.session:
        user = request.user
        productor = Productor.objects.filter(user=user).first()
        periodo = PeriodoCompra.objects.filter(fechaInicio__lte=datetime.datetime.now(),
                                               fechaFin__gte=datetime.datetime.now())
        pedidos = OfertaProductor.objects.filter(productor=productor, periodo=periodo, cantidadVendida__gt=0).order_by(
            '-cantidadVendida')

        # Paginación
        paginator = Paginator(pedidos, 10)
        try:
            pedidos = paginator.page(page)
        except (InvalidPage, EmptyPage):
            pedidos = paginator.page(paginator.num_pages)
        if pedidos.has_previous():
            num_anterior = pedidos.number - 1
        else:
            num_anterior = pedidos.number
        if pedidos.has_next():
            num_siguiente = pedidos.number + 1
        else:
            num_siguiente = pedidos.number
        # JSON
        data_obj = OfertaProductorSerializer(pedidos.object_list, many=True).data

        data = {
            "pedidos": data_obj,
            "num_anterior": num_anterior,
            "num_siguiente": num_siguiente,
            "num_pages": pedidos.paginator.num_pages
        }

    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


# ===========================================================================
# USER VIEWS
# ===========================================================================
def rest_users_productores(request, page):
    productores = Productor.objects.all()

    # Paginación
    paginator = Paginator(productores, 16)
    try:
        productores = paginator.page(page)
    except (InvalidPage, EmptyPage):
        productores = paginator.page(paginator.num_pages)
    if productores.has_previous():
        num_anterior = productores.number - 1
    else:
        num_anterior = productores.number
    if productores.has_next():
        num_siguiente = productores.number + 1
    else:
        num_siguiente = productores.number
    # JSON
    data_obj = ProductorSerializer(productores.object_list, many=True).data
    data = {
        "productores": data_obj,
        "num_anterior": num_anterior,
        "num_siguiente": num_siguiente,
        "num_pages": productores.paginator.num_pages
    }
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_cambiar_estado_productor(request):
    data = {
        "success": False
    }
    if request.method == "POST":
        # Actualiza estado del productor
        id_productor = request.POST.get('idProductor', 0)
        if id_productor > 0:
            productor = Productor.objects.get(id=id_productor)
            productor.activo = not productor.activo
            productor.save()
            data["success"] = True
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_users_actual(request):
    data = {
        'success': False
    }
    if "_auth_user_id" in request.session:
        data = {
            'success': True,
            'user': UserSimpleSerializer(request.user).data,
            'csrf': middleware.csrf.get_token(request)
        }
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_users_cliente_form(request, id_usuario):
    if request.method == "POST":
        if int(id_usuario) > 0:
            user_obj = User.objects.get(pk=id_usuario)
            user_form = UserCreationForm(request.POST, instance=user_obj)
            cliente_obj = user_obj.cliente_set.first()
            cliente_form = ClienteForm(request.POST, instance=cliente_obj)
            data = {
                "success": True,
                "mensaje": u"El usuario ha sido editado"
            }
        else:
            user_form = UserCreationForm(request.POST)
            cliente_form = ClienteForm(request.POST)
            data = {
                "success": True,
                "mensaje": u"El usuario ha sido agregado"
            }
        if user_form.is_valid() and cliente_form.is_valid():
            user_form.save()
            user_obj = user_form.instance
            # Asigna el usuario al grupo 1: consumidor
            user_obj.groups.add(1)
            user_obj.save()
            cliente_obj = cliente_form.instance
            cliente_obj.user = user_obj
            cliente_obj.save()
            logout(request)
            user_obj.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, user_obj)
            request.session["ID_CANASTA"] = 0
        else:
            data = {
                "success": False,
                "mensaje": u"Error en los campos ingresados",
                "user_form": user_form.as_p(),
                "cliente_form": cliente_form.as_p()
            }
    else:
        if int(id_usuario) > 0:
            print int(id_usuario)
            cliente_obj = User.objects.get(pk=id_usuario).cliente_set.first()
            cliente_form = ClienteForm(instance=cliente_obj)
            user_form = UserCreationForm(instance=cliente_obj.user)
        else:
            user_form = UserCreationForm()
            cliente_form = ClienteForm()
        data = {
            "user_form": user_form.as_p(),
            "cliente_form": cliente_form.as_p()
        }
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_users_productor_form(request, id_usuario):
    if request.method == "POST":
        if int(id_usuario) > 0:
            user_obj = User.objects.get(pk=id_usuario)
            user_form = UserCreationForm(request.POST, instance=user_obj)
            productor_obj = user_obj.productor_set.first()
            productor_form = ProductorForm(request.POST, instance=productor_obj)
            data = {
                "success": True,
                "mensaje": u"El usuario ha sido editado"
            }
        else:
            user_form = UserCreationForm(request.POST)
            productor_form = ProductorForm(request.POST)
            data = {
                "success": True,
                "mensaje": u"El usuario ha sido agregado"
            }
        if user_form.is_valid() and productor_form.is_valid():
            user_form.save()
            user_obj = user_form.instance
            # Asigna el usuario al grupo 2: productor
            user_obj.groups.add(2)
            user_obj.save()
            productor_obj = productor_form.instance
            productor_obj.user = user_obj
            productor_obj.save()
            logout(request)
            user_obj.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, user_obj)
        else:
            data = {
                "success": False,
                "mensaje": u"Error en los campos ingresados",
                "user_form": user_form.as_p(),
                "productor_form": productor_form.as_p()
            }
    else:
        if int(id_usuario) > 0:
            productor_obj = User.objects.get(pk=id_usuario).productor_set.first()
            productor_form = ProductorForm(instance=productor_obj)
            user_form = UserCreationForm(instance=productor_obj.user)
        else:
            user_form = UserCreationForm()
            productor_form = ProductorForm()
        data = {
            "user_form": user_form.as_p(),
            "productor_form": productor_form.as_p()
        }
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


# ===========================================================================
# HORARIODISTRIBUCION VIEWS
# ===========================================================================
def rest_horarios_paginado(request, page):
    horarios = HorarioDistribucionSerializer(HorarioDistribucion.objects.all().order_by('diaSemana', 'hora'),
                                             many=True).data

    # Paginación
    paginator = Paginator(horarios, 16)
    try:
        horarios = paginator.page(page)
    except (InvalidPage, EmptyPage):
        horarios = paginator.page(paginator.num_pages)
    if horarios.has_previous():
        num_anterior = horarios.number - 1
    else:
        num_anterior = horarios.number
    if horarios.has_next():
        num_siguiente = horarios.number + 1
    else:
        num_siguiente = horarios.number
    # JSON
    data_obj = HorarioDistribucionPaginadoSerializer(horarios.object_list, many=True).data
    data = {
        "horarios": data_obj,
        "num_anterior": num_anterior,
        "num_siguiente": num_siguiente,
        "num_pages": horarios.paginator.num_pages
    }
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_horario_crear(request):
    data = {}
    if request.method == "POST":
        # Se crea el horario de distribución
        dia_semana = int(request.POST['diaSemana'])
        hora = request.POST['hora']
        HorarioDistribucion.objects.create(diaSemana=dia_semana, hora=hora)
        data["success"] = True
    else:
        data["success"] = False
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_horario_cambiar_estado(request):
    data = {}
    if request.method == "POST":
        # Se crea el item a la canasta
        id_horario = int(request.POST['idHorario'])
        horario = HorarioDistribucion.objects.get(id=id_horario)
        horario.estado = not horario.estado
        horario.save()
        data["success"] = True
    else:
        data["success"] = False
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


# ===========================================================================
# PASARELA PAGO VIEWS
# ===========================================================================
def pagar(request):
    referencia_pago = request.session["ID_CANASTA"]
    valor = 0
    if referencia_pago > 0:
        valor = Carro.objects.get(pk=referencia_pago).valor_total()
    if valor > 0:
        data = {
            'referenciaPago': referencia_pago,
            'valor': valor,
            'fechaPago': now()
        }
        return render(request, 'pagar.html', data)
    else:
        return redirect('index')


def recalcular_unidades(id_producto, cantidad):
    fecha_actual = datetime.datetime.now()
    periodo_actual = PeriodoCompra.objects.filter(fechaInicio__lte=fecha_actual, fechaFin__gte=fecha_actual)
    ofertas_productor = OfertaProductor.objects.filter(producto=id_producto, periodo=periodo_actual.last()).order_by(
        'valorCompra')
    cantidad_restante = cantidad
    for oferta in ofertas_productor:
        unidades_disponibles = (oferta.cantidadCompra - oferta.cantidadVendida)
        if unidades_disponibles > 0:
            if unidades_disponibles >= cantidad_restante:
                oferta.cantidadVendida = oferta.cantidadVendida + cantidad_restante
                oferta.save()
                break
            else:
                oferta.cantidadVendida = oferta.cantidadVendida + unidades_disponibles
                cantidad_restante = cantidad_restante - unidades_disponibles
                oferta.save()


def confirmar_pago(request):
    data = {}
    if request.method == "POST":
        form = TransaccionPasarelaForm(request.POST)
        if form.is_valid():
            id_item_carro = request.session["ID_CANASTA"]
            # Calcula disponibilidad
            disponible = True
            items = ItemCarro.objects.filter(carro=id_item_carro)
            for item in items:
                if item.cantidad > productos_unidades(item.producto.id):
                    print item.producto.producto.nombre
                    disponible = False
                    break
            print disponible
            # Data para el template
            transaccion = form.instance
            transaccion.save()
            data = {
                'transaccionId': transaccion.id,
                'referenciaPago': transaccion.referenciaPago,
                'fechaTx': transaccion.fechaTx,
                'valorTx': transaccion.valorTx
            }
            # Valida disponibilidad
            if disponible:
                transaccion.estado = 'APR'
                data['estado'] = 'APR'
                carro_estado_actualizar(id_item_carro, 'PAG')
                # Recalcula disponibilidad
                for item in items:
                    if item.producto.producto.tipoProducto.id == 2:
                        detalles_producto = DetalleCanasta.objects.filter(canasta=item.producto)
                        for detalle in detalles_producto:
                            recalcular_unidades(detalle.producto.producto.id, detalle.cantidad * item.cantidad)
                    else:
                        recalcular_unidades(item.producto.producto.id, item.cantidad)
            else:
                transaccion.estado = 'REC'
                data['estado'] = 'REC'
                carro_estado_actualizar(id_item_carro, 'REC')
            transaccion.save()
            # Limpia el ID de la canasta
            request.session["ID_CANASTA"] = 0
    return render(request, 'confirmarPago.html', data)


# ===========================================================================
# AUTENTICACIÓN
# ===========================================================================
def canastaorganica_login(request):
    data = {
        'success': False
    }
    # Está solicitando autenticación
    if request.method == "POST":
        username = request.POST['userid']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                id_canasta = request.session["ID_CANASTA"]
                # Cargamos la información de la canasta
                carro_cliente = Carro.objects.filter(user=user, estado='CAN').last()
                if carro_cliente:
                    if id_canasta != 0:
                        carro_temp = Carro.objects.get(id=id_canasta)
                        # se borra la canasta y por el ON CASCADE se borran sus ítems
                        carro_temp.delete()
                    request.session["ID_CANASTA"] = carro_cliente.id
                else:
                    if id_canasta != 0:
                        carro = Carro.objects.get(id=id_canasta)
                        carro.user = user
                        carro.save()
                data = {
                    'success': True
                }
            else:
                data['mensaje'] = "El usuario no está activo"
        else:
            data['mensaje'] = "El usuario o contraseña que ha introducido no es correcto"
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


@login_required(login_url='/login')
def canastaorganica_logout(request):
    data = {
        'success': False
    }
    if request.method == "POST":
        try:
            logout(request)
            request.session["ID_CANASTA"] = 0
            data = {
                'success': True
            }
        except:
            pass
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


# ===========================================================================
# NOTIFICACIONES
# ===========================================================================
def enviar_correos_lista_deseos(request):
    data = {}
    if request.method == "GET":
        # OFERTAS
        fecha_actual = datetime.datetime.now()
        periodo_actual = PeriodoCompra.objects.filter(fechaInicio__lte=fecha_actual, fechaFin__gte=fecha_actual)
        ofertas = Producto.objects.filter(ofertaproducto__periodo=periodo_actual.last())
        # CLIENTES
        clientes = Cliente.objects.all().exclude(productosfavoritos__isnull=True)
        asunto = '[CanastaOrgánica] Disponibilidad de la Semana'
        headers = {'Reply-To': 'misoagilesgrupo3@gmail.com'}
        for cliente in clientes:
            mensaje = u'Hola! \n' \
                      u'\n' \
                      u'Los siguientes productos de tu lista de deseos se encuentran disponibles esta semana en CanastaOrgánica: \n'

            productos_favoritos = cliente.productosfavoritos_set.filter(producto__in=ofertas)
            for producto_favorito in productos_favoritos:
                mensaje += u'-%s \n' % producto_favorito.producto.nombre

            mensaje += u'\n' \
                       u'Feliz Semana :)'
            destino = cliente.user.email
            try:
                email = EmailMessage(asunto, mensaje,
                                     'CanastaOrgánica <misoagilesgrupo3@gmail.com>',
                                     [destino],
                                     [], headers=headers)
                email.send()
                data['success'] = True
            except:
                data['success'] = False
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


# ===========================================================================
# LISTA DE DESEOS
# ===========================================================================
def rest_productos_lista_deseo(request):
    productos = Producto.objects.all()
    data = ProductoSerializer(productos, many=True, context={'request': request}).data

    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_productos_lista_deseo_cambio(request):
    data = {}
    if request.method == "POST":
        id_producto = int(request.POST['id_producto'])
        try:
            cliente = Cliente.objects.get(user=request.user)
            productoFavorito = ProductosFavoritos.objects.get(producto=id_producto, cliente__user=request.user)
            productoFavorito.delete()
            data["success"] = True
            data["delete"] = True
        except ProductosFavoritos.DoesNotExist:
            producto = Producto.objects.get(id=id_producto)
            ProductosFavoritos.objects.create(producto=producto, cliente=cliente)
            data["success"] = True
            data["delete"] = False
        except Exception:
            data["success"] = False

    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_adcicionar_producto_deseado(request):
    data = {}
    if request.method == "POST":
        nombre = request.POST['nombre']
        descripcion = request.POST['descripcion']
        try:
            Solicitudes.objects.create(nombreProducto=nombre, descripcionProducto=descripcion)
            data["success"] = True
        except Exception:
            data["success"] = False

    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


# ===========================================================================
# GESTIÓN PRODUCTOS
# ===========================================================================
def rest_tipo_unidades_lista(request):
    # Objetos
    tipos = TipoUnidad.objects.all()
    paginator = Paginator(TipoUnidad.objects.all(), tipos.count())
    tipos = paginator.page(1)

    # JSON
    data_obj = TipoUnidadSerializer(tipos.object_list, many=True, context={'request': request}).data
    data = {
        "tipos_unidades": data_obj
    }
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_admin_productos_lista(request, page):
    # Objetos
    productos = Producto.objects.all().filter(tipoProducto=1).order_by('id')

    # Paginación
    paginator = Paginator(productos, 6)
    try:
        productos = paginator.page(page)
    except (InvalidPage, EmptyPage):
        productos = paginator.page(paginator.num_pages)
    if productos.has_previous():
        num_anterior = productos.number - 1
    else:
        num_anterior = productos.number
    if productos.has_next():
        num_siguiente = productos.number + 1
    else:
        num_siguiente = productos.number

    # JSON
    data_obj = ProductoSerializer(productos.object_list, many=True, context={'request': request}).data
    data = {
        "productos": data_obj,
        "num_anterior": num_anterior,
        "num_siguiente": num_siguiente,
        "num_pages": productos.paginator.num_pages
    }
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_admin_productos_crear(request):
    data = {}
    if request.method == "POST":
        # Se crea el producto
        nombre = request.POST['nombre']
        descripcion = request.POST['descripcion']
        unidad = request.POST['unidad']
        imagen = request.POST['imagen']

        tipoUnidad = TipoUnidad.objects.get(pk=unidad)

        producto = Producto.objects.create(nombre=nombre, descripcion=descripcion, tipoUnidad=tipoUnidad, foto=imagen)
        producto.save()

        data["success"] = True
    else:
        data["success"] = False
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


def rest_producto_crear_form(request):
    if request.method == "POST":
        form = ProductoForm(request.POST, request.FILES)
        data = {
            "success": True,
            "mensaje": u"El producto ha sido creado"
        }

        if form.is_valid():
            form.save()
        else:
            data = {
                "success": False,
                "mensaje": u"Error en los campos ingresados",
                "form": form.as_p()
            }
    else:
        form = ProductoForm()
        data = {
            "form": form.as_p()
        }
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


# ===========================================================================
# METODO PARA SERVICIOS REST
# ===========================================================================

def autenticar_usuario(username, password):
    mensaje = ""
    status = ""

    try:
        user = User.objects.get(username=username)
        if check_password(password, user.password):
            mensaje = "El usuario ha iniciado sesión"
            status = "OK"
    except:
        mensaje = "Usuario o Clave incorrecta"
        status = "ERROR"

    data = {
        "username": username,
        "status": status,
        "message": mensaje
    }
    return data


def get_periodo_actual():
    fecha = datetime.datetime.now()
    return PeriodoCompra.objects.filter(fechaInicio__lte=fecha, fechaFin__gte=fecha).first()


def get_oferta_producto(periodo):
    ofertas = OfertaProducto.objects.filter(periodo=periodo)
    return ofertas


def get_disponibilidad_producto(id_producto):
    producto = Producto.objects.get(id=id_producto)
    ofertas = OfertaProducto.objects.get(periodo=get_periodo_actual(), producto=producto)
    return productos_unidades(ofertas.id)


def str2bool(v):
    return v.lower() in ("yes", "true", "t", "1")


@api_view(['POST'])
def rest_api_v1_usuario_login(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']

        data = autenticar_usuario(username, password)

        return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


@api_view(['GET'])
def rest_api_v1_listar_producto_ofertados(request):
    if request.method == "GET":

        productores_certificados = Productor.objects.filter(certificado=True)
        productos_certificados = OfertaProductor.objects.filter(productor__in=productores_certificados).values_list(
            'producto', flat=True)
        ofertas = get_oferta_producto(get_periodo_actual())

        page = request.GET.get('page', '')
        if page != "":
            page_min = 10
        else:
            page = 1
            page_min = ofertas.count()

        # Paginación
        paginator = Paginator(ofertas, page_min)
        try:
            ofertas = paginator.page(page)
        except (InvalidPage, EmptyPage):
            ofertas = paginator.page(paginator.num_pages)

        data_obj = OfertaProductoSerializer(ofertas.object_list, many=True, context={'request': request}).data
        productos = []
        for obj in data_obj:
            productoOfertado = {
                "id": obj['producto']['id'],
                "name": obj['producto']['nombre'],
                "price": obj['valorVenta'],
                "unit": obj['producto']['tipoUnidad']['nombre'],
                "premium": obj['producto']['id'] in productos_certificados,
                "image": obj['producto']['foto'],
                "stock": productos_unidades(obj['id']),
            }
            productos.append(productoOfertado)

        return HttpResponse(json.dumps(productos), content_type='application/json; charset=UTF-8')


@api_view(['PUT'])
def rest_api_v1_adicionar_producto_carro(request):
    if request.method == "PUT":
        data = JSONParser().parse(request)

        username = data['username']
        id_product = data['id_product']
        quantity = data['quantity']

        try:
            user = User.objects.get(username=username)

            try:
                carro = Carro.objects.get(user=user, estado="CAN")
            except:
                carro = Carro.objects.create(user=user)
                carro.save()

            producto = Producto.objects.get(id=id_product)
            productoOferta = OfertaProducto.objects.get(producto=producto)
            adicionar_item(carro, productoOferta, quantity)
            status = "OK"
            mensaje = "Agregado al Carrito"
        except:
            status = "ERROR"
            mensaje = "Usuario no existe"

    datos = {
        "status": status,
        "message": mensaje
    }

    return HttpResponse(json.dumps(datos), content_type='application/json; charset=UTF-8')


@api_view(['POST'])
def rest_api_v1_confirmar_pedido(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        confirmation = str2bool(request.POST['confirmation'])

        data = autenticar_usuario(username, password)
        if data.get("status") == 'OK':
            user = User.objects.get(username=username)
            carro = Carro.objects.get(user=user, estado="CAN")
            if confirmation:
                items = ItemCarro.objects.filter(carro=carro)
                confirmar = False
                for item in items:
                    if get_disponibilidad_producto(item.producto.id) > 0:
                        confirmar = True
                    else:
                        item.delete()
                if confirmar:
                    carro_estado_actualizar(carro.id, "PAG")
                    status = "OK"
                    mensaje = "Confirmada la compra."
                else:
                    status = "ERROR"
                    mensaje = "Producto Agotado."
            else:
                status = "OK"
                mensaje = "Operación cancelada."
                carro_estado_actualizar(carro.id, "REC")
        else:
            status = "ERROR"
            mensaje = data.get("message")

        data = {
            "status": status,
            "message": mensaje
        }

    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


@api_view(['GET'])
def rest_api_v1_listar_horarios_distribucion(request):
    datos = []
    if request.method == "GET":
        periodo = get_periodo_actual()
        horarios_set = HorarioDistribucion.objects.filter(estado=True)

        numdays = (periodo.fechaFin - periodo.fechaInicio).days
        for x in range(0, numdays):
            fechaNueva = periodo.fechaInicio + datetime.timedelta(days=x)
            for horario in horarios_set:

                if fechaNueva.weekday() == int(horario.diaSemana):
                    dato = {
                        "id": horario.id,
                        "description": fechaNueva.strftime('%Y-%m-%d ') + str(horario.hora)
                    }
                    datos.append(dato)

    return HttpResponse(json.dumps(datos), content_type='application/json; charset=UTF-8')


@api_view(['GET'])
def rest_api_v1_listar_productos_comprados(request):
    if request.method == "GET":
        datos = []
        username = request.GET.get('username', '')

        if username != '':
            user = User.objects.get(username=username)

            try:
                periodo = get_periodo_actual()
                carros = Carro.objects.filter(user=user, estado="PAG", fechaCreacion__gte=periodo.fechaInicio,
                                              fechaCreacion__lt=periodo.fechaFin)
                items = ItemCarro.objects.filter(carro__in=carros)

                for item in items:
                    dato = {
                        "id": item.producto.producto.id,
                        "name": item.producto.producto.nombre,
                        "quantity": item.cantidad,
                        "price": item.producto.valorVenta,
                        "unit": item.producto.producto.tipoUnidad.nombre,
                        "total": item.cantidad * item.producto.valorVenta
                    }
                    datos.append(dato)
            except:
                datos = {
                    "message": "Error consultando pedidos"
                }
        else:
            datos = {
                "message": "El usuario no existe"
            }

        return HttpResponse(json.dumps(datos), content_type='application/json; charset=UTF-8')
