# -*- coding: utf-8 -*-
from django.contrib.auth.models import User, Group
from rest_framework import serializers

from CanastaOrganica.models import OfertaProducto, TipoUnidad, Producto, ItemCarro, HorarioDistribucion, \
    PeriodoCompra, OfertaProductor, Productor, Carro, DetalleCanasta, TipoProducto, ProductosFavoritos

__author__ = 'Juan David García Manrique'


# ===========================================================================
# SERIALIZERS
# ===========================================================================
class TipoUnidadSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoUnidad
        fields = ('id', 'nombre')


class TipoProductoSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoProducto
        fields = ('id', 'nombre')


class HorarioDistribucionSerializer(serializers.ModelSerializer):
    diaSemana = serializers.SerializerMethodField()

    class Meta:
        model = HorarioDistribucion
        fields = ('id', 'diaSemana', 'hora', 'estado')

    def get_diaSemana(self, obj):
        return obj.get_diaSemana_display()


class HorarioDistribucionPaginadoSerializer(serializers.ModelSerializer):
    class Meta:
        model = HorarioDistribucion
        fields = ('id', 'diaSemana', 'hora', 'estado')

    def get_diaSemana(self, obj):
        return obj.get_diaSemana_display()


class ProductoSerializer(serializers.ModelSerializer):
    tipoUnidad = TipoUnidadSerializer(read_only=True)
    tipoProducto = TipoProductoSerializer(read_only=True)
    favorito = serializers.SerializerMethodField('producto_favorito')

    def producto_favorito(self, producto):
        favorito = False
        try:
            if "_auth_user_id" in self.context['request'].session:
                user = self.context['request'].user
                productoFavorito = ProductosFavoritos.objects.get(producto=producto, cliente__user=user)
                favorito = True
        except ProductosFavoritos.DoesNotExist:
            favorito = False
        except KeyError:
            favorito = False

        return favorito

    class Meta:
        model = Producto
        fields = ('id', 'nombre', 'descripcion', 'tipoUnidad', 'tipoProducto', 'foto', 'favorito')


class OfertaProductoSerializer(serializers.ModelSerializer):
    producto = ProductoSerializer(read_only=True)

    class Meta:
        model = OfertaProducto
        fields = ('id', 'producto', 'valorVenta')


class ItemCarroSerializer(serializers.ModelSerializer):
    producto = OfertaProductoSerializer(read_only=True)

    class Meta:
        model = ItemCarro
        fields = ('id', 'cantidad', 'producto')


class UserSimpleSerializer(serializers.ModelSerializer):
    groups = serializers.StringRelatedField(read_only=True, many=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'groups')


class PeriodoCompraSerializer(serializers.ModelSerializer):
    class Meta:
        model = PeriodoCompra
        fields = ('id', 'fechaInicio', 'fechaFin')


class ProductorSerializer(serializers.ModelSerializer):
    user = UserSimpleSerializer(read_only=True)

    class Meta:
        model = Productor
        fields = ('id', 'certificado', 'user', 'activo', 'telefono')


class OfertaProductorSerializer(serializers.ModelSerializer):
    productor = ProductorSerializer(read_only=True)
    producto = ProductoSerializer(read_only=True)

    class Meta:
        model = OfertaProductor
        fields = ('id', 'valorCompra', 'cantidadCompra', 'cantidadVendida', 'productor', 'producto')


class CarroSerializer(serializers.ModelSerializer):
    user = UserSimpleSerializer(read_only=True)
    horario_distribucion = HorarioDistribucionSerializer(read_only=True)
    valor_total = serializers.IntegerField()

    class Meta:
        model = Carro
        fields = ('id', 'fechaCreacion', 'user', 'horario_distribucion', 'estado', 'valor_total')
        read_only_fields = ('valor_total',)


class DetalleCanastaSerializer(serializers.ModelSerializer):
    canasta = OfertaProductoSerializer(read_only=True)
    producto = OfertaProductoSerializer(read_only=True)

    class Meta:
        model = DetalleCanasta
        fields = ('id', 'canasta', 'producto', 'cantidad')


class UsuarioSerializerAPI(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'password')
