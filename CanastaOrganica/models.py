# coding=utf-8
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator
from django.core.validators import MinValueValidator
from django.db import models
from django.db.models import F, Sum, DecimalField
from django.utils.translation import ugettext as _


# PARÁMETROS DEL SISTEMA
class PeriodoCompra(models.Model):
    fechaInicio = models.DateTimeField()
    fechaFin = models.DateTimeField()

    def __unicode__(self):
        return u'%s-%s' % (str(self.fechaInicio), str(self.fechaFin))


DAY_OF_THE_WEEK = {
    '1': _(u'Monday'),
    '2': _(u'Tuesday'),
    '3': _(u'Wednesday'),
    '4': _(u'Thursday'),
    '5': _(u'Friday'),
    '6': _(u'Saturday'),
    '7': _(u'Sunday'),
}


class DayOfTheWeekField(models.CharField):
    def __init__(self, *args, **kwargs):
        kwargs['choices'] = tuple(sorted(DAY_OF_THE_WEEK.items()))
        kwargs['max_length'] = 1
        super(DayOfTheWeekField, self).__init__(*args, **kwargs)


class HorarioDistribucion(models.Model):
    diaSemana = DayOfTheWeekField()
    hora = models.TimeField()
    estado = models.BooleanField(default=True)

    def __unicode__(self):
        return u'%s-%s' % (self.diaSemana, str(self.hora))


# CLIENTE
class Cliente(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    direccion = models.CharField(max_length=200)
    telefono = models.BigIntegerField(validators=[MinValueValidator(1000000),
                                                  MaxValueValidator(9999999999)])

    def __unicode__(self):
        return u'%s' % self.user.username


# PRODUCTO
class TipoUnidad(models.Model):
    nombre = models.CharField(max_length=100)

    def __unicode__(self):
        return u'%s' % self.nombre


class TipoProducto(models.Model):
    nombre = models.CharField(max_length=100)

    def __unicode__(self):
        return u'%s' % self.nombre


class Producto(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=500)
    tipoUnidad = models.ForeignKey(TipoUnidad)
    tipoProducto = models.ForeignKey(TipoProducto, default=1)
    foto = models.ImageField(upload_to='fotos', default="fotos/organic.png")
    timestamp = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u'%s' % self.nombre


class OfertaProducto(models.Model):
    producto = models.ForeignKey(Producto)
    periodo = models.ForeignKey(PeriodoCompra)
    valorVenta = models.PositiveIntegerField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u'%s-%s' % (self.periodo, self.producto.nombre)


class DetalleCanasta(models.Model):
    canasta = models.ForeignKey(OfertaProducto, related_name='detallecanasta_canasta')
    producto = models.ForeignKey(OfertaProducto, related_name='detallecanasta_producto')
    cantidad = models.IntegerField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u'%s-%s' % (self.canasta, self.producto)


# PRODUCTOR
class Productor(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    certificado = models.BooleanField(default=False)
    telefono = models.BigIntegerField(validators=[MinValueValidator(1000000),
                                                  MaxValueValidator(9999999999)])
    activo = models.BooleanField(default=True)

    def __unicode__(self):
        return u'%s' % self.user.username


class EstadoOferta(models.Model):
    nombre = models.CharField(max_length=10)

    def __unicode__(self):
        return u'%s' % self.nombre


class OfertaProductor(models.Model):
    productor = models.ForeignKey(Productor)
    periodo = models.ForeignKey(PeriodoCompra)
    producto = models.ForeignKey(Producto)
    valorCompra = models.PositiveIntegerField()
    cantidadCompra = models.PositiveIntegerField()
    cantidadVendida = models.PositiveIntegerField(default=0)
    estado = models.ForeignKey(EstadoOferta)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u'%s-%s-%s' % (self.periodo, self.productor.user.username, self.producto.nombre)


# PASARELA
class TransaccionPasarela(models.Model):
    referenciaPago = models.CharField(max_length=30)
    valorTx = models.CharField(max_length=20)
    # APR = Aprobada, REC = Rechazada
    estado = models.CharField(max_length=3, default='APR')
    fechaTx = models.DateTimeField(auto_now_add=True)


# CARRO DE COMPRAS
class Carro(models.Model):
    fechaCreacion = models.DateField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    horario_distribucion = models.ForeignKey(HorarioDistribucion, null=True)
    # CAN = Canasta, PEN = Pendiente, PAG = Pagado, REC = Rechazado
    estado = models.CharField(max_length=3, default='CAN')

    def __unicode__(self):
        return u'%s' % self.user.username

    def valor_total(self):
        valor_items = 0
        try:
            valor_items = int(ItemCarro.objects.filter(carro=self.id).aggregate(
                total=Sum(F('cantidad') * F('producto__valorVenta'), output_field=DecimalField()))['total'])
        except TypeError:
            # Lanza TypeError cuando no hay ítems en el carro
            pass
        return valor_items


class ItemCarro(models.Model):
    cantidad = models.IntegerField()
    carro = models.ForeignKey(Carro, on_delete=models.CASCADE)
    producto = models.ForeignKey(OfertaProducto)


# LISTA DE DESEOS
class ProductosFavoritos(models.Model):
    cliente = models.ForeignKey(Cliente)
    producto = models.ForeignKey(Producto)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u'%s-%s' % (self.cliente, self.producto)


# LISTA DE DESEOS PRODUCTOS NO EXISTENTES
class Solicitudes(models.Model):
    nombreProducto = models.CharField(max_length=60)
    descripcionProducto = models.CharField(max_length=250)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u'%s-%s' % (self.nombreProducto)
