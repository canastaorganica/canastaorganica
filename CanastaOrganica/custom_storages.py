# -*- coding: utf-8 -*-
from django.conf import settings
from storages.backends.s3boto import S3BotoStorage

__author__ = 'Juan David García Manrique'


# class StaticStorage(S3BotoStorage):
#     location = settings.STATICFILES_LOCATION


class MediaStorage(S3BotoStorage):
    location = settings.MEDIAFILES_LOCATION
